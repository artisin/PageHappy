/******************************************************************************/
/******************************************************************************/
/*  Controller: none 
/*  Template: /client/views/
/******************************************************************************/
/******************************************************************************/

/***************************************************************/
/* Template States */
/***************************************************************/
/*******************************/
// Created
/*******************************/
Template.spotlight.created = function () {
  //Creates instance
  this.pHappy = CreatePageHappy({
    pagerMasterId: 'spotlight',
    collection: 'People',
    pageSize: 15,
    catchLimit: 20000,
    //insight just create some
    //sessions for the count
    insight: true,
    spotlight:{
      funk: function (doc) {
        console.log(arguments)
        return true;
      }
    },
    querySelector: {
      // "email.verified": true
    },
    querySort: {
      number: 1
    },
  });
};

/*******************************/
// Rendered
/*******************************/
Template.spotlight.rendered = function () {

};

/*******************************/
// Destroyed
/*******************************/
Template.spotlight.destroyed = function () {
  this.pHappy.destroy();
};


/***************************************************************/
/* Template Helpers */
/***************************************************************/
Template.spotlight.helpers({
  pHappyContext: function () {
    var pHappy = Template.instance().pHappy;
    return pHappy;
  },
  subReady: function () {
    var pHappy = Template.instance().pHappy;
    return pHappy.subReady();
  },
  pHappyData: function () {
    var pHappy = Template.instance().pHappy;
    return pHappy.render(); 
  },
  spotlighter: function () {
    // console.log(this)
    //'this' of course is the doc object
    return this.spotlight ? 'spotlight' : '';
  }
});


/***************************************************************/
/* Template Events */
/***************************************************************/
Template.spotlight.events({
  'click .spEven': function (e, tmpl) {
    e.preventDefault();
    var pHappy = tmpl.pHappy;
    pHappy.changeSetting('spotlight', {
      even: function (doc) {
        return doc.number % 2 === 0;
      },
      singlePage: true
    }, true);
  },
  'click .spTrue': function (e, tmpl) {
    e.preventDefault();
    var pHappy = tmpl.pHappy;
    pHappy.changeSetting('spotlight', {
      true: function (doc) {
        console.log(this)
        return doc.email.verified === true;
      },
    }, true);
  },
  'click .spName': function (e, tmpl) {
    e.preventDefault();
    var pHappy = tmpl.pHappy;
    pHappy.changeSetting('spotlight', {
      name: function (doc) {
        return doc.firstName.length > 4;
      },
    }, true);
  },
});