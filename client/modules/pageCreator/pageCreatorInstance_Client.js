CreatePageHappy = function (options) {
  function PageHappy () {};
  PageHappy.prototype = pagerCreatorProtoype;
  var f = new PageHappy();
  f.init(options);
  return f;
};

LocalPH = new Mongo.Collection('localPh');
CountPH = new Mongo.Collection('countPh');

var self = this;
_PH = {
  _GLOBAL: {}
};

var pagerCreatorProtoype = {
  init: function (options) {
    console.log('INIT RAN')
    //////////************************ uncomment
    // var _PC;
    // var pagerClientObserver = null;
    var defaults = {
      masterId: undefined,
      collection: undefined,

      //Defualt, need some work on this
      retainLocalCopy: false,
      //TODO: create on setup 
      retainRef: [],




      pageSize: 25,
      currentPage: 0,

      /*-----------------------------*/
      /// Auto Update
      /*-----------------------------*/
      //Will update records on add/change/remove
      autoUpdate: false,
      //Throttle time.
      //As such it will only allow autoUpate to be called once every (x)
      //which in turn will poll the collection for changes
      // autoUpdatePollPeriod: 3000,

      // observer

      /*-----------------------------*/
      /// PreFetch
      /*-----------------------------*/
      //Pre-fetches next pages
      preFetch: false,
      //How many sets will be prefetched
      //Default 1 === preFetch neighboors
      preFetchDeviation: 6,


      //** if preFetch false -> need to be false
      preFetchBatch: true,
      //TODO -- set to zero is preFEtchBatch is false, not needed but lets keep it clean
      //TODO --- preFetchDeviation must be larger than the preFetchBatchSize, recomend twise
      //the size
      preFetchBatchSize: 3,



      //TODO --- Set preFetchBatch to true if this is true
      preFetchAll: false,
      preFetchAllBatchSize: 25,
      //Default -> 0
      preFetchAllBatchTimeIntv: 0,



      ///**** preFetchDeviation > preFetchBatch
      //** if preFetch false -> need to be false
      //Alright we about to get a little crazy here.
      //So if a document is changed and now falls withing the preFetch perview
      //but not the current page, it will be added although when the user goes
      //to that page the data will reflect that of the old data untill the 
      //autoUpatePollPeriod has expired and the call is made to the server which 
      //it will then realize the data is incorrect and procced to change that data. 
      //So everything works. As expexted.
      //Nevertheless, if true this it will proceed to check all prefetched pages
      //to make sure that the data is correct. 
      autoUpdatePreFetch: false,
      
      //Throttle invervolt
      autoUpdatePreFetchDebounce: 3000,
      //Will intelegently check in batches, so lets say you got the following
      //prefetched [1,2,3,4,5,6], [11,12,13,14], [33,32,34,36,37]
      //it will prefetch theses groups thus making only three calls to theee server yo.
      preFetchCheckBatch: false,
      //
      preFetchCheckBatchSize: 10, 




      //Based on pageSize (2*25pageSize = 50)
      //Depermins the inital sub limit, once data is then catched
      //the sub limit will defualt to the catchLimit
      initSubLimit:  2,
      
      //To normilize limit
      baseLimit: 100,
      

      //These index will remain in the catch and will not be cleaned
      //last indes if last arg === true
      // ensureCatch: [0, 1, 2, true],
      ensureCatch: [0, 1, 2, true],

      //If no user, a uniq id is assinged into the global PH object
      userId: Meteor.userId ? Meteor.userId() : undefined, 
      // collectionObserverId: undefined,

      totalRecords: 0,
      users: Meteor.user !== undefined ? true : false,
      
      //Querys
      selector: {},
      sort: {},
      fields: {},
      
      //Real time filter?????????
      queryFilter: {},
      

      spotlight: {},

      queryRange: true,
      method: 'getPagerAsync',

      pass: 0,


      // currentCatchSize: 0,
      // cleanCollectionCatch: false,
      // catchLimit: 500,

      //Size is based on pageSize
      //Such as (pageSize: 10, catchCleanerSize: 2) = 20
      //So when the catch limit is reached 20 will be removes
      ////FIX withi limit
      // catchCleanerSize: 2,

      //Determins how we will clean catch
      //Two options:
      //page - cleans the pages furthes away from current
      //timestamp - cleanst the oldest
      // cleanCatchBy: 'page',

      // cleanCollectionOnLoad: false,

      //Ref to all the trackers since they need to be stoped
      //on the destroyed call
      trackers: [],

      //
      insight: false,
      //Will override any defualt settings
      overRide: false
    };

    //Extend this, to merge
    _.extend(this, defaults, options);
    // debugger

    var x = this.masterId;
    _PH[x] = this;

    var _Ph = _PH[x]; 
    _Ph.initSetUpComplete = new ReactiveVar(false);




    //*---------------------------------------------*/
    // Init Setup Process
    /*---------------------------------------------*/
    ASQ()
      //Create New Local Collection
      .then(function createLocalCol (done) {
        _Ph.ID = _Ph.masterId;
        _Ph.redis = new Miniredis.RedisStore(_Ph.masterId+'-collection');
        done();
      })
      //Run init setup
      .then(function initSetup(done) {
        var sq = ASQ();
        sq
          .seq(_Ph.initSetup())
          .val(function complete () {
            //Parent done
            return done();
          });
      })
      .then(initSubscription)
      .then(function update (done) {
        var sq = ASQ();
        sq
          .seq(_Ph.update())
          .val(function complete () {
            //parent done
            return done();
          });
      })
      .then(function (done) {
        _Ph.initSetUpComplete.set(true);
        //Activates Trackers And such;
        done(pagerWatch());
      })
      .or(function (err) {
        console.warn('Init Error!');
        console.warn(err);
      });



      function initSubscription (done) {
  
        /*-----------------------------*/
        /// Collection counter sub
        /*-----------------------------*/
        var t6 = Tracker.autorun(function () {
          //Static
          var params = {};
          params.collection = _Ph.collection;
          params.selector = _Ph.query.get('selector');
          params.masterId = _Ph.masterId;
          //Sub
          _Ph.countHanndle = Meteor.subscribe('collectionCount', params);
          //Count cursor
          var count = CountPH.findOne(_Ph.masterId);
          if (count) {
            _Ph.totalRecords.set(count.count);
          }
        });
        if (_Ph.retainLocalCopy) {
          _Ph.retainRef.push(t6);
        }

        _Ph.trackers.push(t6);
        done();

        /*-----------------------------*/
        ///Collection data sub
        /*-----------------------------*/
        var t5 = Tracker.autorun(function () {
          //Sub Trigger
          _Ph.initSetUpComplete.get();
          //Static
          var params = {};
          params.collection = _Ph.collection;
          params.masterId = _Ph.masterId;
          params.sort = _Ph.query.get('sort');
          params.selector = _Ph.query.get('selector');
          params.userId = _Ph.userId;
          params.retainLocalCopy = _Ph.retainLocalCopy;
          //Rec
          // params.limit = _Ph.obvSubLimit.get();
          //Sub
          _Ph.collectionHandle = Meteor.subscribe('localPager', params);
        });
        //If the user wants to retain the local copy of the db we must
        //then take note of the id so when the destroy funk runs it
        //will not stop the tracker just pause it.
        if (_Ph.retainLocalCopy) {
          _Ph.retainRef.push(t5);
        }
        _Ph.trackers.push(t5);
      }



      /*---------------------------------------------*/
      // Auto Run Trackers
      /*---------------------------------------------*/
      function pagerWatch () {




        /*-----------------------------*/
        /// Observer Limit Setter
        /*-----------------------------*/
        var t4 = Tracker.autorun(function () {
          var pageSize = _Ph.pageSize.get(),
              pagesCatched = _Ph.pagesCatched.get().length,
              base = _Ph.baseLimit,
              limit = pagesCatched  * pageSize,
              normalizedLimit = limit + (base - (limit % base));
          _Ph.obvSubLimit.set(normalizedLimit);
        });
        _Ph.trackers.push(t4);


        var t9 = Tracker.autorun(function (c) {
          console.log('RENDER DATA ============> Change')
          var referenceIds = _Ph.referenceIds.get();
          if (c.firstRun) {
            return;
          }else{
            _Ph.renderDataGenerator(referenceIds);
          }
        });
        _Ph.trackers.push(t9);


      /*-----------------------------*/
      /// Stats -> for insight
      /*-----------------------------*/
      if (_Ph.insight) {
        var t8 = Tracker.autorun(function () {
          //Collection

          //Pages
          var pagesLoaded = _Ph.catchedIds.keys('*'),
              pageNum = _.map(pagesLoaded, function (key) {
                return Number(key.replace( /^\D+/g, ''));
              }),
              current = _Ph.currentPage.get();
          Session.set('pHappy_pagesLoaded', pageNum);
          Session.set('pHappy_currentPage', current);
        });
        _Ph.trackers.push(t8);
      }


      /*-----------------------------*/
      /// Auto Update Checker
      /// Calls method to check for changes
      /*-----------------------------*/
      // var autoUpdateCheck = _.throttle(autoUpdate, _PC.autoUpdatePollPeriod);
      // function autoUpdate () {
      //   return _PC.autoUpdateCheck();
      // }
        


      /*-----------------------------*/
      /// Pager Observer
      /*-----------------------------*/
      _Ph.clientObserver = LocalPH
      .find({masterId: _Ph.masterId}, {sort: {number: 1}})
      .observe({
        //added??????
        addedAt: function (doc, atIndex, before) {
          console.log('ADDED');
          // console.log(arguments)
            var referenceId = doc.referenceId,
                id = doc._id,
                fields = _.omit(doc, '_id');


            var insertDoc = function (done) {
              var insertFields = JSON.stringify(fields);
              // console.log(insertFields);
              _Ph.redis.set(referenceId, insertFields);
              done();
            };

            var MGR = ASQ();
            MGR
              //In no autoupdate is running
              .then(function noAutoUpdate (done) {
                if (!_Ph.autoUpdate) {
                  insertDoc(done);
                  MGR.abort();
                }else{
                  done();
                }
              })
              .then(function autoUpdate (done) {
                //Before is always `null` unless the doc is
                //added via the mongoHndle in the pub
                if (doc.changed) {
                  //Add new doc
                  insertDoc(done);
                  //Check its place
                  var refIds = _Ph.referenceIds.get(),
                      id = doc._id;
                  console.log(refIds)
                  console.log(id)
                  console.log(before)
                  if (_.contains(refIds, before)) {
                    //Added on current page
                    refIds.splice(_.indexOf(refIds, before), 0, id);
                    refIds.length = _Ph.pageSize.get();
                    _Ph.referenceIds.set(refIds); 
                  }else{
                    console.log('RUN PREFETCH UPDATE');
                  }
                }else{
                  //defualt
                  insertDoc(done);
                }
              })
              .or(function (err) {
                if (!_Ph.overRide) {
                  console.warn('Observer Added Error!');
                  console.warn(err);
                  console.warn('If you would like to remove these error warnings you can add "override: true" to this specific instance');
                }
              });
        },
        movedTo: function (doc, fromIndex, toIndex, before) {
          console.log('MOVED TO')
          console.log(arguments)

          if (before) {
            var refIds = _Ph.referenceIds.get(),
                id = doc._id;
            //Check to see if moved doc exsists in current list
            if (_.contains(refIds, doc._id)) {
              //If in current list we just rearange the docs
              refIds.splice(fromIndex, 1);
              refIds.splice(_.indexOf(refIds, before), 0, id);
            }else{
              //If not, insert the doc and then cut off the end
              //since the list will be in order
              refIds.splice(_.indexOf(refIds, before), 0, id);
              refIds.length = _Ph.pageSize.get();
              //And of course we then must check the preFetch data
              _Ph.preFetchCheck();
            }
            _Ph.referenceIds.set(_.flatten(refIds));
          };
        },
        changed: function (doc, oldDoc) {
          console.log('CHANGED')
          var id = doc._id;

          var findDoc = function () {
            var doc = LocalPH.findOne(id);
            doc = _.omit(doc, '_id');
            console.log(doc)
            return doc;
          };

          var MGR = ASQ();
          MGR.defer()
            .then(function (done) {
              if (!_Ph.autoUpdate) {
                MGR.abort();
              }
              done();
            })
            //Get doc info
            .val(findDoc)
            .then(function changeDoc(done, doc) {
              //Only update doc if found
              if (_.isEmpty(doc)) {
                MGR.abort();
                done();
              }
              done(doc);
            })
            //Update redis
            .then(function updateDoc (done, doc) {
              var rId = doc.referenceId,
                  formatedDoc = JSON.stringify(doc);
              //Overright redis doc with new doc info
              _Ph.redis.set(rId, formatedDoc);
              done();
            })
            .or(function (err) {
              if (!_Ph.overRide) {
                console.warn('Observer Changed Error!');
                console.warn(err);
                console.warn('If you would like to remove these error warnings you can add "override: true" to this specific instance');
              }
            });
        },
        removed: function (doc) {
          console.log('REMOVED')
          console.log(arguments)
          // console.log(arguments)
        }, 
      });

    }
  },
  /*--------------------------------------------------------------------------*/
  /*--------------------------------------------------------------------------*/
  // Update Params for method calls
  // @return - params
  /*--------------------------------------------------------------------------*/
  updateParams: function (rangeQuery) {
    var _Ph = this,
        params = {
          collection: _Ph.collection,
          userId: _Ph.userId,
          masterId: _Ph.masterId,
          selector: _Ph.query.get('selector'),
          sort: _Ph.query.get('sort'),
          fields: _Ph.query.get('fields'),
          limit: _Ph.obvSubLimit.get(),
          pageSize: _Ph.pageSize.get(),
          currentPage: _Ph.currentPage.get(),
          users: _Ph.users,
        };

    //TimeStamp, do I need???
    if (_Ph.cleanCatchBy === 'timestamp') {params.pagerTimestamp = +new Date();}

    if (rangeQuery) {
      //Get selector
      var mongoSelector = _Ph.helpers().mongoSelector(params.currentPage);
      params = _.extend(params, mongoSelector);
      return params;
    }else{
      return params;
    }
  },

  //*--------------------------------------------------------------------------*/
  /*--------------------------------------------------------------------------*/
  // Spotlighter
  /*--------------------------------------------------------------------------*/
  spotlighter: function (fields) {
    var _Ph = this,
        originVal = _Ph.spotlight.get(), 
        settings = _.pluck(originVal, 'current', 'all', 'singlePage', 'page'),
        spotlight = _.omit(originVal, 'current', 'all', 'singlePage', 'page');
// debugger
    /*-----------------------------*/
    /// Config
    /*-----------------------------*/
    var configure = function () {

      //Setters//
      var spotlightType = null;
      //Custom function
      if (spotlight.customFunctions) {
        spotlightType = customFunctions;
      }
      //_.contains
      if (spotlight.contains) {
        spotlightType = contains;
      }
      //Disable all spotlight
      if (_.isEmpty(spotlight) && settings.all) {
        spotlightType = none;
      }

      //Checkers//
      //Simple check to make sure we have atleast one truth
      var oneTruth = _.map(spotlight, function (val) {
        return val;
      });
      //If there is not one truthy
      if (!_.contains(oneTruth, true) && !_.isEmpty(spotlight)) {
        //Check if only funks
        var isFunks = _.map(oneTruth, function (val) {
          return _.isFunction(val);
        });
        //True if only funks
        if (_.every(isFunks, _.identity)) {
          spotlightType = customFunctions;
        }else if(_.isObject(spotlight.customFields)){
          spotlightType = null;
        }else{
          throw 'Sorry but I could not determin what you are trying to do. Try to speficy what your are using; such as "customeFunctions: true" otherwise I get all confuzed.';
        }
      }
      
      if (spotlight.contains && spotlight.customFunctions) {
        throw 'Unfortunately, at this time you have you can not use both custom functions and contains. Although, you can create a custom function that uses _.contains and that should worlk swell.';
      }
      return spotlightType;
    };


    //*-----------------------------*/
    /// Custom Function
    /*-----------------------------*/
    var customFunctions = function (fields) {
      var funkExsists = _.map(spotlight, function (val) {
        return _.isFunction(val);
      });
      //Throw err if no funk
      if (!_.contains(funkExsists, true)) {
        throw 'No functions found, yet you specified that this is what you want to do. Make is so, and add a function with a boolean outcome';
      }

      /**
       * Runs through spotlight functions
       * @return - modified fields to reflect that of spotlight
       */
      var funkCheck = _.map(spotlight, function (val, indvFunk) {
        if (_.isFunction(val)) {
          return spotlight[indvFunk](fields);
        } 
      });
      //Clean
      funkCheck = _.without(funkCheck, undefined);
      //Set the new value for spotlight
      fields.spotlight = _.every(funkCheck, _.identity);
      //->
      return fields;
    };


    //*-----------------------------*/
    /// Underscore Contains
    /*-----------------------------*/
    var contains = function (fields) {
      var list,
          listType = (function () {
            if (spotlight.containsList !== undefined) {
              list = spotlight.containsList;
              return 'one';
            }else if(spotlight.containsAllList !== undefined){
              list = spotlight.containsAllList;
              return 'all';
            }
          })();
      //Throw error if no list
      if (list === undefined) {
        throw 'No containsList or containsAllList field found. Make is so, in order to go.';
      }
      var containsCheck = _.map(list, function (val) {
        if (_.contains(fields, val)) {
          return true;
        }
        return false;
      });

      //Contains one
      if (listType === 'one') {
        if (_.contains(containsCheck, true)) {
          fields.spotlight = true;
        }
      }
      //Contains All
      if (listType === 'all') {
        if (_.every(containsCheck, _.identity)) {
          fields.spotlight = true;
        }
      }
      return fields;
    };


    //*-----------------------------*/
    /// Custome Fields
    /*-----------------------------*/
    var customFields = function (field) {
      var funks = spotlight.customFields;
      if (funks === undefined) {
        throw 'No customFields object found! Make it so, to procced.';
      }

      var checkFunks = function () {
        _.each(funks, function (val, index, list) {
          if (val.equals === undefined) {
            throw 'No "equals" field found in your customFields! Make it so, to procced.';
          }
          var hasFunk = _.map(val, function (indVal) {
            if (_.isFunction(indVal)) {
              return true;
            }
          });
          //Clean
          hasFunk = _.compact(hasFunk);
          if (!_.contains(hasFunk, true)) {
            throw 'No function found in one of your customFields! Make it so, to procced.';
          }
        });
        return funks;
      };

      var runFunks = function (funkObj, field) {

        _.each(funkObj, function (obj, newField) {
          field[newField] = null;

          //Grab funk
          var conditionFunk = _.map(obj, function(subFunk) {
            if (_.isFunction(subFunk)) {
              return subFunk;
            }
          });
          conditionFunk = _.first(_.compact(conditionFunk));
          //Grab condition
          var conditionToApply = _.map(obj, function (subCondition) {
            if (!_.isFunction(subCondition)) {
              return subCondition;
            }
          });
          conditionToApply = _.first(_.compact(conditionToApply));


          var equalsType = null;
          //One outcome check
          if (_.isString(conditionToApply)) {
            equalsType = 'one';
          }
          //Boolean outcome check
          if (_.isObject(conditionToApply)) {
            var cond = conditionToApply;
            if (cond.true === undefined) {
              throw 'Shit son, you gotta have a truth outcome';
            }
            if ( cond.false === undefined) {
              throw 'Shit son, you gotta have a false outcome.';
            }
            equalsType = 'boolean';
          }

          //Funk outcome
          var funkOutcome = conditionFunk(field);
          //
          if (equalsType === 'boolean') {
            if (funkOutcome) {
              field[newField] = conditionToApply.true;
            }else{
              field[newField] = conditionToApply.false;
            }
          }
          //
          if (equalsType === 'one') {
            if (funkOutcome) {
              field[newField] = conditionToApply;
            }
          }
        });
        return field;
      };

      return ASQ()
        .val(checkFunks)
        .val(function (funkObj) {
          return runFunks(funkObj, field);
        })
        .or(function (err){
          console.warn('Custome Highlight Fields Error!');
          console.warn(err);
        });

    };


    var none = function () {
      var cleanCurrentPage = function (done) {
        var currentIds = _PC.referenceIds.get(),
            sharedCollection = Pager.find({referenceId: {$in: currentIds}}); 
        sharedCollection.forEach(function (doc) {
          var referenceId = doc.referenceId,
              localCopy = LocalCol[_PC.ID].findOne({referenceId: referenceId}, {fields: {_id: 1}});
          var newDoc = _.omit(doc, '_id');
          LocalCol[_PC.ID].update(localCopy, newDoc);
        });
        done();
      };

      var cleanRest = function (done) {
        Meteor.setTimeout(function () {
          var selector = _PC.obvSelector.get(),
              sharedCollection = Pager.find(selector); 
          sharedCollection.forEach(function (doc) {
            var referenceId = doc.referenceId,
                localCopy = LocalCol[_PC.ID].findOne({referenceId: referenceId}, {fields: {_id: 1}});
            var newDoc = _.omit(doc, '_id');
            LocalCol[_PC.ID].update(localCopy, newDoc);
          });
          done();
        }, 0);
      };


      ASQ()
        .then(cleanCurrentPage)
        .then(cleanRest)
        .or(function (err) {
          console.warn('Splotlight Cleaner Error!');
          console.log(err);
        });

    };

    //*-----------------------------*/
    /// Highlight Reset
    /*-----------------------------*/
    var spotlightReconfig = function () {
      //Reset data
      if (_.isEmpty(spotlight) && settings.all) {
        none();
      }else{
        var spotlightFields = function (currentIds) {
          //Timeout needed????????????
          //
            var convertBack = function (done, id, newFields) {
              // console.log(arguments)
              // console.log(id)
              newFields = JSON.stringify(newFields);
              // console.log(newFields)
              var converted = redis[_PC.ID].set(id, newFields);
              // console.log(redis[_PC.ID].get(id))
              if (converted) {
                done();
              }
            };

            return ASQ()
              .val(configure)
              .then(function reconFig (done, funk) {
                var sq = ASQ();
                sq
                  .map(currentIds, function (id, done) {
                    var fields = JSON.parse(redis[_PC.ID].get(id)),
                        newFields = funk(fields);
                    return convertBack(done, id, newFields);
                  })
                  .val(function completeReconFig() {
                    //Parent Done
                    return done();
                  });
              });

        };

        var MGR = ASQ();
        MGR.defer()
          //Highlight current page first
          .then(function currentPage (done) {
            var currentIds = _PC.referenceIds.get();
            var sq = ASQ();
            sq
              .seq(spotlightFields(currentIds))
              .val(function currentPageComp () {
                //Parent Done
                return done();
              });
          })
          .then(function updateRender (done) {
            var sq = ASQ();
            sq
              .seq(_PC.renderDataGenerator())
              .val(function () {
                //parent done
                done();
              });
          })
          //And then the rest
          .then(function restOfPages (done) {
            if (_PC.spotlight.get().all) {
              debugger
            }else{
              done();
            }
          })
          .or(function (err){
            console.warn('Spotlight Reset Error!');
            console.warn(err);
          });  
      }

    };



    /*-----------------------------*/
    /// Main Manager
    /*-----------------------------*/
    var MGR = ASQ();
    return MGR.defer()
      .then(function resetNeeded(done) {
        if (fields === undefined) {
          spotlightReconfig();
          MGR.abort();
        }
        done();
      })
      .val(configure)
      .val(function (funk) {
        if (funk === null) {
          console.log('funk addCustomFields callled')
          //If user is only using 'addCustomFields'
          return fields;
        }else{
          return funk(fields);  
        }
      })
      .val(function (fields) {
        if (spotlight.addCustomFields || _.isObject(spotlight.customFields)) {
          var sq = ASQ();
          return sq
            .seq(customFields(fields));
        }
        return fields;
      })
      .val(function (fields) {
        return fields;
      });
  },


  /*--------------------------------------------------------------------------*/
  /*--------------------------------------------------------------------------*/
  // Update
  /*--------------------------------------------------------------------------*/
  /**
   * Update
   * @param  {boolean} resetCatch - if true will bypass and reset catch (hard update)
   * otherwise it will proceed as normal, preFetching
   * @return Once updated it will procceed to call preFetch
   */
  update: function (resetCatch) {
    // console.log('UPDATE');
    var _Ph = this;

    var getParams = function () {
      return _Ph.updateParams();
    };

    /**
     * Determins if the catch will be used
     * @return - {boolean} useCatch - if true catch will be use 
     * @return - [id's] - ids ref list to be used
     */
    var checkCatch = function () {
      _Ph.useCatch.set(false);
      // debugger
      var catchSet;
      //This var is specified in update call
      resetCatch = resetCatch === undefined ? false : true;

        if (!resetCatch) {
          var key = "p-"+_Ph.currentPage.get(),
              exsist = _Ph.catchedIds.exists(key);
          if (exsist) {
            catchSet = _Ph.catchedIds.lrange(key, 0, -1);
          }
        }else{
          debugger;
          //Hard upate reset all catched data since no longer reflective/actucate
          // _Ph.catchedDataList.set([]);
          // _Ph.catchedData.set([]);
          // _Ph.pagesCatched.set([]);
          // //If update pre fetch is actice
          // if (_Ph.autoUpdatePreFetch) {
          //   //Set temp block since ids will be in flux and will produce a false/postive
          //   _Ph.tempPreFetchCheckBlock.set(true);
          // }
        }

      return catchSet;
    };


    var useCatchSet = function (done, catchSet) {
      _Ph.useCatch.set(true);
      _Ph.complete.set('update', false);
      //Use local catch if set and avalible
      console.log('USE CATCH===========>');
      //Set the ids to use for the catched data
      _Ph.referenceIds.set(catchSet);
      //Set PAges
      _Ph.setPages();
        if (_Ph.preFetch) {
          //Fetches the next set, with said prams
          _Ph.catchPages(_Ph.updateParams());
        }
      _Ph.complete.set('update', true);
      done();
    };

    var fetchDataFromServer = function (done, params) {
      console.log('NOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO');
      _Ph.useCatch.set(false)
      _Ph.complete.set('update', false);
      var mongoSelector = _Ph.helpers().mongoSelector(params.currentPage);
      params = _.extend(params, mongoSelector);
      Meteor.apply(_Ph.method, [params], true, function (err, res) {
        if (err) {
          throw err;
        }else{
          _Ph.referenceIds.set(res.referenceIds);
          // _Ph.setPages();
          _Ph.complete.set('update', true);
          //Call-> preFetch / catch data
          var preFetchData = {
            currentPage: params.currentPage,
            referenceIds: res.referenceIds,
          };
          _Ph.catchPages(params, preFetchData);
          done();
        }
      });
    };


    /**
     * Only called on first pass (init)
     * This ensurs proper load, since we are using apply it will wait until
     * a return until any other methods get called
     */
    var firstPass = function (done, params) {
      _Ph.complete.set('update', false);
      //If it is the first pass through we must set pages after the result
      //otherwise the setPages is handled in the changePage funk
      //note: no need for range since first call limited
      Meteor.apply(_Ph.method, [params], true, function (err, res) {
        if (err) {
          console.log(err);
        }else{
          //IMPORTATN, we need to indicate completion for anything to be renderd
          done();
          
          //Waits for data to be observed before taking action 
          Tracker.autorun(function (c) {
            var keyCount = _Ph.redis.matching('*').count();
            if (keyCount >= _Ph.pageSize.get()) {
              _Ph.referenceIds.set(res.referenceIds);
              _Ph.complete.set('update', true);
              _Ph.complete.set('init', true);
              
              //Set Pages
              _Ph.setPages();
              
              //Catch/Storte data
              var preFetchData = {
                currentPage: params.currentPage,
                referenceIds: res.referenceIds,
              };
              _Ph.catchPages(params, preFetchData);
              
              //Top tracker
              c.stop();
            }
          });

        }
      });
    };


    /**
     * Funk-Manager for update;
     * First time through, it will take the path of initPass which
     * relays its completion back to the init method. 
     * Additionaly, this method will alway call _Ph.catchPages
     * to catch/store said data.
     */
    /*---------------------------------------------*/
    // Update Manager
    /*---------------------------------------------*/
    var MGR = ASQ(),
        initPass = _Ph.pass <= 1;

    /**
     * Inital pass
     */
    if (initPass) {
      return MGR
        .val(getParams)
        .then(function (done, params) {
          firstPass(done, params);
        })
      .or(function (err) {
        console.warn('Page-Happy: Error in update call during the First-Pass!');
        console.warn(err);
      });
    }

    /**
     * Regual Pass
     */
    if (!initPass) {
      return MGR
        .val(checkCatch)
        .then(function (done, catchSet) {
          var sq = ASQ();
          //Use Catch
          if (catchSet) {
            sq
              .then(function (done) {
                useCatchSet(done, catchSet);
              })
              .val(function complete () {
                //Parent Done
                return done();
              })
              .or(function (err) {
                if (err) {
                  console.warn('PageHappy: Error in upate call in `useCatchSet`');
                  console.warn(err);
                }
              });
          }else{
            //No catchSet so we must go fetch it off the server
            sq
              .val(getParams)
              .then(function (done, params) {
                fetchDataFromServer(done, params);
              })
              .val(function complete () {
                //Parent complete
                done();
              })
              .or(function (err) {
                if (err) {
                  console.warn('PageHappy: Error in upate call in `fetchDataFromServer`');
                  console.warn(err);
                }
              });
          }
        })
      .or(function (err) {
        console.warn('Page-Happy: Error in update call!');
        console.warn(err);
      });
    }



  },

/**
 * (M) - Pre-fetches pages
 * @param  {obj} params
 */
  catchPages: function (params, preFetchData) {
    var _Ph = this;

    /**
     * #1
     * Initial Catch Check
     * catchPages, can be called with a data context to set with catchIds
     * so if it is this will set it
     */
    function initCatch () {
      if (params !== undefined && preFetchData !== undefined) {
        return ASQ()
          .then(function (done) {
            catchIds(done, preFetchData);
          });
      }
      return ASQ()
        .then(function (done) {
          done();
        });
    }

    /**
     * #2
     * Establishes params
     * @return {params}
     */
    function establishParams () {
      //Set Params if not defined
      if (params === undefined) {
        params = _Ph.updateParams();
      }
      return params;
    }

    /**
     * Stores/creates ref to catched ids.
     * This bugger is the fucking key to the who preFetch gig
     * -sets - catchedData - which holds an array ref or Id for said page for lookup
     * -sets - pagesCatched - which is a simple array of which pages are catched
     * 
     * @param  {obj} params for method call
     * @param  {res} preFetchData - res from a preFetch call
     * @return invokes preFetch to fetch
     */
    function catchIds (done, data, batchMode) {
      var preFetchActive = data !== undefined ? true : false;
          batchMode = batchMode !== undefined ? true : false;
      /**
       * Inserts the doc into redis
       * Adds ref to pages catched
       * @param  {[page number]} key
       * @param  {[idSet]} value 
       */
      var insertDoc = function (key, value) {
        var formateKey = "p-"+key,
            catchExsists = _Ph.catchedIds.exists(formateKey);
        //Edge case check if users get page happy can push more than one set
        if (!catchExsists) {
          //Push new ids to set
          _.each(value, function(id){
            if (data.reverse) {
              _Ph.catchedIds.lpush(formateKey, id);
            }else{
              _Ph.catchedIds.rpush(formateKey, id);
            }
          });
          //Update pages catched
          var tempPagesCatched = _.union(_Ph.pagesCatched.get(), key);
          _Ph.pagesCatched.set(tempPagesCatched);
        }
      };

      //Batch Mode
      if (batchMode) {
        _.each(data.currentPage, function (pageNum) {
          var key = pageNum,
              value = data.pageIds.splice(0, _Ph.pageSize.get());
          insertDoc(key, value);
        });
      }

      //Non-batchMode
      if (!batchMode) {
        var key = preFetchActive ? data.currentPage : _Ph.currentPage.get(),
            value = preFetchActive ? data.referenceIds : _Ph.referenceIds.get();
        insertDoc(key, value);
      }

      //Send data to then be removed from waitList
      done(data.currentPage);
    }
    

    /**
     * #3
     * Generates a Queue List
     * @return - pushing queue to _Ph.preFetchQueue
     */
    function generateQueue () {
      var currentCatch = _Ph.pagesCatched.get(),
          currentPage = _Ph.currentPage.get(),
            deviation = _Ph.preFetchDeviation;
        
        //*-----------------------------*/
        /// Helpers
        /*-----------------------------*/
        /**
         * Helper- cleans list
         * @param  {[pages]} list of pages
         * @return {[pages]} -> Cleaned
         */
        var cleanList = function (list) {
          //Clean the list, so there is no duplicate of records
          var cleanedList = _.map(list, function (val) {
            if (!_.contains(currentCatch, val)) {
              return val;
            }
          });
          //If it has zero we got to do a bit of a walk around
          if (_.contains(cleanedList, 0)) {
            //Compact removes zero
            cleanedList = _.compact(cleanedList);
            //So lets add it back on
            cleanedList.push(0);
            return cleanedList;
          }
          return _.compact(cleanedList);
        };
        /**
         * Helper - Checks to see if pages have been catched
         * so that pages will not be quened that are already catched
         * @param  {[numbers]} array
         * @return {boolean} vals have to all be true to return false
         */
        var catchCheck = function (array) {
          var contain = _.map(array, function (val) {
            return _.contains(currentCatch, val);
          });
          return !_.every(contain, _.identity);
        };

        /**
         * Helper - Creates divation list - like standard divation
         * @param  {number} page
         * @param  {number} amount to divate by
         * @param  {1 or -1} num - create a positive of neg list
         * @return {[numbers]} 
         */
        var createDivation = function (page, amount, num) {
          var divationList = [];
          for (var i = 1; i <= amount; i++) {
            var next = i * num;
            //Make sure no neg or bigger then max
            if ((page + next) >= 0 && (page + next) <= _Ph.maxPageNumber) {
              divationList.push(page + next);
            }
          }
          return divationList;
        };



        /**
         * Finds pages that need to be put in queue list
         * @return {[nums]} - Of pages that need to be fetched
         */
        var createQueueList = function () {
          var preFetchQueue = [];

          //Ensure catchList to be catched
          var ensureCatchList = function (done) {
            //Remove true out of array
            var ensureCatch = _.without(_Ph.ensureCatch, true);
            //Check ensureCatch
            if (catchCheck(ensureCatch)) {
              var ensureCatchQueue = ensureCatch;
              ensureCatchQueue = cleanList(ensureCatchQueue);
              for (var i = 0; i < ensureCatchQueue.length; i++) {
                preFetchQueue.push(ensureCatchQueue[i]);
              }
            }
            done();
          };

          //Next Page's to be catched
          var nextList = function (done) {
            var nextList = createDivation(currentPage, deviation, 1);
            if (catchCheck(nextList) && !_Ph.lastPage) {
              nextList = cleanList(nextList);
              for (var i = 0; i < nextList.length; i++) {
                preFetchQueue.push(nextList[i]);
              }
            }
            done();
          };

          //Previous Page's to be catched
          var prevList = function (done) {
            var prevList = createDivation(currentPage, deviation, -1);
            if (catchCheck(prevList) && !_Ph.firstPage) {
              prevList = cleanList(prevList);
              for (var i = 0; i < prevList.length; i++) {
                preFetchQueue.push(prevList[i]);
              }
            }
            done();
          };

          /**
           * When user want to preFetch every page this will just generate
           * an array of every page that is not yes preFetched
           */
          var queueAll = function (done) {
            for (var i = 0; i < _Ph.maxPageNumber; i++) {
              //No need to catch what is already catched
              if (!_.contains(currentCatch, i)) {
                if (_Ph.preFetchAllBatchSize === 0) {
                  preFetchQueue.push(i);
                }else{
                  //preFetchAllBatchs
                  if (_Ph.preFetchAllBatchSize >= preFetchQueue.length) {
                    preFetchQueue.push(i);
                  }
                }
              }
            }
            done();
          };


        /*-----------------------------*/
        /// createQueueList sub-manager -> #3.a
        /*-----------------------------*/
        var subMGR = ASQ();
        return subMGR
          .then(function (done) {
            if ( _Ph.complete.equals('preFetch', false)) {
              //If the current prefetch is not complete abort, since it will
              //create duplicates of the data and waste client/server resorces.
              //This really only is used if the user is trigger-fucking-happy
              //and is requesting new pages as fast as thier little finger
              //can click.
              console.log('ABORT==========================');
              subMGR.abort();
            }else{
              done();
            }
          })
          .then(function (done) {
            //To 
            if (_Ph.preFetchAll && _Ph.pass > 1) {
              queueAll(done);
            }else{
              var sq = ASQ();
              sq
                .gate(
                  ensureCatchList,
                  nextList,
                  prevList
                )
                .then(function () {
                  //Parent -> done
                  done();
                });
            }
          })
          .val(function removeDuplicates () {
            return _.uniq(preFetchQueue);
          })
          .or(function (err) {
            console.warn('Page-Happy: Error in Pre-Fetch!');
            console.warn(err);
          });
      };


      /*-----------------------------*/
      /// generateQueue Manager -> #3
      /// This is a bit wierd how I set this up 
      /// but in doing it this way I was able
      /// to create private helpers
      /*-----------------------------*/
      var MGR = ASQ();
      return MGR
        .seq(createQueueList); //#3.a
    }


    /**
     * #4
     * Gets/Sets preFetch data
     * @param  {[queueList]} pageList to be queue/preFetched
     */
    function fetchQueue (queueList) {

        /**
         * Makes a call to server to publish preFetch data
         */
        var fetchDataFromServer = function (done, pgNum, mongoSelector, batchMode) {
          params.currentPage = pgNum;
          //Range Query
          if (_.isString(mongoSelector.lastRangeId)) {
            params.queryRange = true;
            params.lastRangeId = mongoSelector.lastRangeId;
            params.rangeKey = mongoSelector.rangeKey;
          }else{
            //Skip Query
            params.queryRange = false;
          }

          //BatchMode
          if (batchMode) {
            params.batchMode = true;
          }

          Meteor.call(_Ph.method, params, function (err, res) {
            if (err) {
              console.warn(err);
            }else{
              done(res);
            }
          });
        };


        /**
         * #4.a
         * Process'es the queueList and then sets the data into redis
         * @param  {[]} queueList Pages to be fetched from the server
         * @param  {boolean} batchMode -> to indicate we will be using batches
         * @return done() -> to tell the fetch Queue manager that we have
         * fetched and set the queue list
         */
        var processQueue = function (queueList, batchMode) {
          // console.log(queueList)
          //Call ->
          var MGR = ASQ();
          return MGR
            //Manages Single Queue
            .then(function singleQueue (done) {
              if (batchMode) {
                done(); 
              }else{
                var sq = ASQ();
                //Iterates through queueList, and gets/sets data
                _.each(queueList, function (pgNum, index, list) {
                  //Call ->
                  sq
                    //Creates mongo selectro range query or skip
                    .val(function createMongoSelector () {
                      return _Ph.helpers().mongoSelector(pgNum);
                    })
                    //Get data
                    .then(function (done, rangeData) {
                      fetchDataFromServer(done, pgNum, rangeData);
                    })
                    //Sets data
                    .then(function (done, res) {
                      var data = {};
                      data.currentPage = res.pgNum;
                      data.referenceIds = res.referenceIds;
                      data.reverse = res.reverse;
                      //Sets local catch refferance
                      catchIds(done, data);
                    })
                    //Indicated completion
                    .val(function comp () {
                      if (index+1 === list.length) {
                        //Parent -> Done
                        return done();
                      }
                    });
                });
              }
            })
            //Manages the batch queue
            .then(function batchQueue (done) {
              if (!batchMode) {
                done();
              }else{
                ASQ()
                  //Create batch queue
                  .seq(_Ph.helpers().createBatchQueue(queueList))
                  .then(function (done, batchQueueList) {
                    // console.log(batchQueueList)
                    var sq = ASQ();
                    //Iterates through batches, and gets/sets data
                    _.each(batchQueueList, function (batch, index, list) {
                      sq
                        .val(function createMongoSelector () {
                          return _Ph.helpers().mongoSelector(batch);
                        })
                        .then(function (done, batch) {
                          var batchList = batch.batchList;
                          fetchDataFromServer(done, batchList, batch, true);
                        })
                        //Set Data
                        .then(function (done, res) {
                          var data = {};
                          data.currentPage = batch;
                          data.pageIds = res.referenceIds;
                          data.reverse = res.reverse;
                          //Set catch
                          catchIds(done, data, batchMode);
                        })
                        //Indication completion
                        .val(function complete () {
                          if (index+1 === list.length) {
                          //parent -> done
                          return done();
                          }
                        });
                    });
                  })
                  .val(function batchQueueComplete () {
                    //Parent -> done
                    return done();
                  }); 
              }
            })
            .or(function (err) {
              console.warn('Page-Happy: Error in Pre-Fetch -> fetchQueue -> processQueue!');
              console.warn(err);
            });
        };
        

        /**
         * #4.b
         * This handles preFetching all said records. It acts as a closed loop.
         * @param  {Function} done -> for fetch queue Manager
         * @param  {[]} queueList this is the inital queue list such as the
         * ensuered-index, since we want to inially prefetch as little data
         * as possible for the quickes load time
         * @return done() to indicate to the fethc queue manager it is complete
         */
        function preFetchAll (done, queueList) {
            /**
             * Loops through batches untill all are preFetched
             */
            var nextBatchDone;
            var nextBatchLoop = function (done) {
              //Assinge a reffrence to done, sice nextBatchLoop will keep on
              //calling its self untill there is nothing left. 
              if (_.isFunction(done)) {
                nextBatchDone = done;
              }
              return ASQ()
                .seq(generateQueue)
                .then(function preFetchLoop (done, queueList) {
                  //Queue till there no more
                  if (queueList.length !== 0) {
                    var sq = ASQ();
                    return sq
                      //We pass true here so it know we would like to preFetch
                      //in batches
                      .seq(processQueue(queueList, true))
                      .val(function () {
                        //Keep on fetching till there ani't nothing left
                        Meteor.setTimeout(function () {
                          nextBatchLoop();
                          //Defualt this inverval is set at zero unless specified
                          //otherwise
                        }, _Ph.preFetchAllBatchTimeIntv);
                      });
                    }else{
                      //preFetchLoop -> done
                      done();
                    }
                })
                //Indicate completeion of nextBatchLoop
                .val(function () {
                  return nextBatchDone();
                })
                .or(function (err) {
                  console.warn('Page-Happy: Error in Pre-Fetch All Next Batch Loop!');
                  console.warn(err);
                });
              };

          //Keep on fetching till there ani't nothing left
          return ASQ()
            //This process is the init preFetch, such as ensure indexs so that
            //we fetch those before quene all the other pages, for a silky
            //smooth transition
            .then(function processInit (done) {
              var sq = ASQ();
                sq
                .seq(processQueue(queueList))
                .val(function () {
                  //We need to short these vars, since the rest of the prefetching
                  //will be handled here
                  //Take a look at createQueueList sub-manager -> #3.a for a ref
                   _Ph.complete.set('preFetch', true);
                  if (_Ph.pass === 1) {
                    _Ph.pass++;
                  }
                  return done();
                });
            })
            //Start the nextBatchLoop to prefetch all pages
            .then(function (done) {
              var sq = ASQ();
                sq
                  .seq(nextBatchLoop(done));
            })
            //Inicate preFetchAll is done fetching all
            .val(function () {
              //preFetchAll -> master -> done
              return done();
            })
            .or(function (err) {
              if (err) {
                console.warn('Page-Happy: Error in Pre-Fetch All!');
                console.warn(err);
              }
            });
        }



        /*-----------------------------*/
        ///Fetch Queue Manager #4
        /*-----------------------------*/
        var MGR = ASQ();
        return MGR
          //Type of queue
          //Batch or single
          .then(function (done) {
            if (!_Ph.preFetchBatch) {
              done(false);
            }else{
              //Batch Trigger
              //Once we hit said limit, batch q
              if (queueList.length >= _Ph.preFetchBatchSize) {
                done(true);
              }else{
                //Otherwise we say preFetch is complete since we have not hit
                //the batch trigger there is nothing to be fetched
                 _Ph.complete.set('preFetch', true);
              }
            }
          })
          //Proccess Queuee
          .then(function (done, batchMode) {
            var sq = ASQ();
            if (!_Ph.preFetchAll) {
              //Normal process
              sq
                .seq(processQueue(queueList, batchMode)) //#4.a
                .val(function () {
                  //parent  done
                  return done();
                });
            }else{
              //preFetchAll Process
              sq
                .seq(preFetchAll(done, queueList)) //#4.b
                .val(function () {
                  //parent -> done
                  return done();
                });
            }
          })
        .then(function (done) {
          //Fetch Queue Manager -> done
          done();
        })
        .or(function (err) {
          if (err) {
            console.warn('Page-Happy Error');
            console.warn(err);
          }
        });
      }


    //*-----------------------------*/
    /// CatchPages Controller
    /*------------------------------*/
    var Controller = ASQ();
    return Controller
      //#1
      .then(function init (done) {
        initCatch().pipe(done);
        if (!_Ph.preFetch) {
          Controller.abort();
        }
      })
      .val(establishParams) //#2
      .then(function (done) {
        var sq = ASQ();
          sq
            .seq(generateQueue) //#3
            .val(function (queueList) {
              //Parent -> done
              return done(queueList);
            });
      })
      .then(function (done, queueList) {
        if (queueList.length > 0) {
          //Set ->
           _Ph.complete.set('preFetch', false);
          //Call->
          ASQ()
            .seq(fetchQueue(queueList)) //#4
            .then(function () {
               _Ph.complete.set('preFetch', true);
              //Parent -> done
              done();
            });
        }else{
          //Set ->
           _Ph.complete.set('preFetch', true);
          //Parent -> done
          done();
        }
      })
      .then(function comp (done) {
        //CatchPages Controller -> Maser -> done
        done();
      })
      .or(function (err) {
        console.warn('Page-Happy: Pre-Fetch Error!');
        console.warn(err);
      });
  },

  /*--------------------------------------------------------------------------*/
  /*--------------------------------------------------------------------------*/
  // Reffrance Bellow
  /*--------------------------------------------------------------------------*/
  preFetchCheckManager: function (keys) {
    var _PH = this;
    
    var keyNumbers = _.map(keys, function (key) {
      return Number(key.replace( /^\D+/g, ''));
    });
    var getCurrent = function () {
      return _PH.preFetchCheckList.get();
    };

    var MGR = ASQ();
    return MGR 
      .val(keyNumbers)
      .val(function (keyNums) {
       return _.union(keyNums, getCurrent()); 
      })
      .then(function complete (done, pagesToCheck) {
        _PH.preFetchCheckList.set(pagesToCheck);
        done();
      })
      .or(function (err) {
        console.warn('Page-Happy: PreFetchCheck Manager Error!');
        console.error(err);
      });
  },
  /*--------------------------------------------------------------------------*/
  /*--------------------------------------------------------------------------*/
  // Checks preFetched data sets and changes it if needed
  // Its origin lays within _Ph.autoUpdateCheck.
  // From there the current _Ph.catechedData is sent to _Ph.preFetchCheckManager
  // which happns to be the funk righ above us. From 0there its added to a call list
  // which yeah I know what you are thinking. WTF is going on here. Well you can
  // rest asure I am not all sure if this is dumb or brilliant or somewhere in 
  // between. Nevertheless, there is a reason for this maddness. Back to the story.
  // This call list is in a autoTracker which is throttled thrus reducing uneeded
  // checks and does not run if we are actually doing some real preFetching.
  // Anyways, the whole reason we do this is becuase if a document is changed 
  // and now falls withing the preFetch perview but not the current page, 
  // the chances will be reflected in the Pager collection but not the local.
  // And since 
  // @return - description 
  /*--------------------------------------------------------------------------*/
  
  preFetchCheck: function () {
    var _Ph = this;
    
    //#1
    var createBatchQueue = function () {
      //Array of pages that are currenty catched in redis
      var catchedPages = _Ph.helpers().cleanRedisKeys(_Ph.catchedIds.keys('*'));
      return ASQ()
        .seq(_Ph.helpers().createBatchQueue(catchedPages));
    };


    /**
     * #2
     * Makes call to compare and share data sets
     * @param  {ob} dataToCheck - {page#, referenceIds}
     * @return {Callback}             [description]
     */
    function checkData (done, queueList) {
      
      var config = function (list) {
        var params = _Ph.updateParams();
        console.log(list)
        //Check if 
        if (_.contains(list.pages, 0)) {
          params.initList = true;
          params.list = list;
        }else{
          // var key = "p-" + _.last(list.pages),
          //     pageSize = params.pageSize,
          //     lastRangeId = _.first(_PC.catchedData.lrange(key, (pageSize - 1), pageSize));
          // if (lastRangeId) {
          //   var rangeSelector = {
          //     lastRangeId: lastRangeId,
          //     rangeKey: '$gt',
          //     rangeLimit: pageSize * list.count
          //   };
          //   return _.extend(params, rangeSelector);
          // }
        }
        return params;
      };
      
      var compareData =  function (params) {
        debugger
        //Extract varibles from optiosn
        var selector = params.selector,
            options = {
              sort: params.sort || {},
              fields: {_id: 1},
              skip: params.currentPage * params.pageSize,
              limit: params.pageSize
          };

          //BatchMode set
          if (params.batchMode) {
            options.skip = _.first(params.currentPage) * params.pageSize;
            options.limit = (params.currentPage.length * params.pageSize);
            console.log(options);
          }

          var currentIdSet = LocalPH.find(selector, options),
              idsToCheck = params.idsToCheck,
              newIdSet = currentIdSet.map(function (doc) {
                return doc._id;
              });

          var compareIdList = function (arr1, arr2) {
            if (arr1.length === arr2.length) {
              var matchPairs = [];
              for (var i = 0; i < arr1.length; i++) {
                matchPairs.push(arr1[i] === arr2[i]);
              }
              //Will only return true if all pairs match
              return _.every(matchPairs, _.identity);
            }else{
              return false;
            }
          };

          if (compareIdList(idsToCheck, newIdSet)) {
            return {
              setNewPrefetch: false
            };
          }else{
            return {
              setNewPrefetch: true,
              newIdSet: newIdSet,
              currentPage: params.currentPage,
              batchMode: params.batchMode
            };
          }

      };

      var changeData = function (res) {
        debugger
          //No need to set new prefetch
          if (!res.setNewPrefetch) {
            console.log('=================> No')
            return done();
          }
          
          /*-----------------------------*/
          /// If preFetched data needs to be changed
          /*-----------------------------*/

          var insertDoc = function (key, newValue) {
            console.log('====================> CHANGE DATA');
            var formateKey = "p-"+key;
            //Clear old current prefetch refferance
            _Ph.catchedIds.ltrim(formateKey, -1, 0);
            //Set new id list
            _.each(newValue, function (id) {
              _Ph.catchedIds.rpush(formateKey, id);
            }); 
          };

          //BatchMode
          if (res.setNewPrefetch && res.batchMode) {
            _.each(res.currentPage, function (pageNum) {
              var key = pageNum,
                  value = res.newIdSet.splice(0, _Ph.pageSize.get());
              insertDoc(key, value);
            });
            return done();
          }

          //Non-BatchMode
          if (res.setNewPrefetch && !res.batchMode) {
            console.log('====================> CHANGE DATA');
            insertDoc(res.currentPage, res.newIdSet);
            return done();
          }
        };

        return ASQ()
          .val(function () {
            return config(queueList);
          })
          .val(function (params) {
            return _Ph.helpers().getIdList(params);
          })
          
          // .val(function (params) {
          //   console.log('herherererer')
          //   console.log(arguments)
          //   compareData(params);
          // })
          // // .then(changeData(done))
          // .val(function () {
          //   console.log(arguments)
          // })

    }

    //*-----------------------------*/
    /// preFetchCheck Manager
    /*------------------------------*/
    var MGR = ASQ();
    MGR
      //Set watch var
      .then(function (done) {
        //REMOVE ??????????????????????????????????
         _Ph.complete.set('preFetchCheck', false);
        done();
      })
      .then(function (done) {
        console.log(1)
        var sq = ASQ();
        sq
          .seq(createBatchQueue())
          .val(function (batchList) {
            console.log(2)
            console.log(batchList)
            // return done(batchList);
          });
      })
      .then(function queueManager (done, queueList) {
        var sq = ASQ();
          return sq
            //Needed to map otherwise it will throw err
            .then(function initMap (done) {
              queueList = _.toArray(queueList);
              done(queueList); 
            })
            .map(function queueManager (queueList, done) {
              checkData(done, queueList);
            })
            .val(function complete() {
              //Parent -> done
              done();
            });

      })
      .then(function complete (done) {
        //Reset list
        _Ph.preFetchCheckList.set([]);
        _Ph.complete.set('preFetchCheck', true);
        done();
      })
      .or(function (err) {
        console.warn('Page-Happy: Main PreFetchCheck Error!')
        console.warn(err);
      });
  }, 


  //Create a list of setting changes
  changeSetting: function (setting, newValue, extend) {
    var _Ph = this,
        settingVal = _Ph[setting],
        oldVal,
        changeAction,
        keepSetting,
        varType;


    var config = function (done) {
      //Checks
      if (!_.isString(setting)) {
        throw 'A change in settings only accepts string values. Make is so to procced.'; 
      }
      if (settingVal === undefined) {
        throw 'Hmm, no setting called: '+setting+' was found. Are you use you spelled the setting right? Otherwise, it looks like you are out of luck since there is not much I can do with undefined';
      }

      //Special change types
      // if (setting === 'pageSize') {extendChange = false};
      if (setting === 'spotlight') {
        changeAction = 'spotlighter';
        //Stash settings
        keepSetting = _.pick(settingVal.get(), 'current', 'all', 'singlePage');
        //Using pairs since it disconnects the reffrnace to the recVar
        //maybe not the best way to do so but it works
        //used for the originalSpotlight
        oldVal = _.pairs(settingVal.get());
      }

      //Determins var type for change
      if (settingVal instanceof ReactiveVar) {
        varType = 'reactive';
      }else{
        varType = 'static';
      }

      
      done();
    };

    var isObject = function () {
      var check = _.map(arguments, function (val){
        var list = [];
        list.push(_.isObject(val));
        list.push(!_.isFunction(val));
        list.push(!_.isArray(val));
        return list;
      });
      return _.every(check, _.identity);
    };

    //Set-> new settings
    var setRective = function (done) {
      var valid = !_.isEmpty(newValue) || _.isNumber(newValue) || _.isBoolean(newValue);
      var clean = function () {
        if (_.isArray(oldSetting)) {
          settingVal.set([]);
        }else if (_.isObject(oldSetting)){
          settingVal.set({});
        }else{
          settingVal.set(undefined);
        }
      };

      //Extend if specified
      if (extend) {
        //Using pairs to disconnect ref
        var oldSetting = _.pairs(settingVal.get())
        //New value
        if (valid) {
          if (!isObject(oldSetting, newValue)) {
           _Ph.helpers().warning('Extending with an array value which I will try to do but the it prbly will not turn out as planed. Stick to extending objects only for best result.');
          }
          var newSetting = _.deepExtend(newValue, _.object(oldSetting));
          settingVal.set(newSetting);
          console.log(newValue)
        }else{
          //If new val empty assume to be cleaned
          clean();
        }
      }else{
        //Set new val
        if (valid) {
          settingVal.set(newValue);
        }else{
          //If new val empty assume to be cleaned
          clean();
        }
      }
      done();
    };

    //Set static var
    var setStatic = function (done) {
      var oldSetting = settingVal,
          valid = !_.isEmpty(newValue) || _.isNumber(newValue) || _.isBoolean(newValue);
      var clean = function () {
        if (_.isArray(oldSetting)) {
          _Ph[setting] = [];
        }else if(_.isObject(oldSetting)){
          _Ph[setting] = {};
        }else{
          _Ph[setting] = undefined;
        }
      };

      if (extend) {
        //New value
        if (valid) {
          if (!_.isObject(oldSetting) || !_.isObject(newValue)) {
           errorHelper();
          }
          var newSetting = _.extend(oldSetting, newValue);
          _Ph[setting] = newSetting;
        }else{
          //If new val empty assume to be cleaned
          clean();
        }
      }else{
        //New value
        if (valid) {
          _Ph[setting] = newValue;
        }else{
          //If new val empty assume to be cleaned
          clean();
        }
      }
      done();
    }; 

    var actionCall = function (done) {
      
      //Reconfig spotlight
      if (changeAction === 'spotlighter') {
        var currentVal = settingVal.get(),
            currentPage = _Ph.currentPage.get(),
            oldValObj = _.object(oldVal),
            withSettings = _.deepExtend(currentVal, oldValObj, keepSetting);
      
          //Alright its about to get a bit harry here
          //We need to keep a original copy and temp due to the singlePage
          //feture. Thus, we got to know what persists and what disapears.
          if (_Ph.originalSpotlight) {
            //If singlePage, we do not want to apply to orignal since that
            //is going to be the val on the next page and if it for a single instancevv
            if (!newValue.singlePage) {
              var extendObj = _.deepExtend(newValue, oldValObj);
              _Ph.originalSpotlight.set(extendObj);
            }
          }else{
            //Init, so we need to set the page, if the page chnages this will
            //be deleted, refer to the renderDataGenerator and checkout
            //the MGR to get a better idea what I be talking about
            if (!newValue.singlePage) {
              newValue.page = currentPage;
              _Ph.originalSpotlight = new ReactiveVar(newValue);
            }
            
          }
          settingVal.set(withSettings);
          _Ph.spotlighter()
        }
      
      done();
    };


    ASQ()
     .then(config)
     .then(function set (done) {
       if (varType === 'reactive') {
        setRective(done);
       }
      if(varType === 'static'){
        setStatic(done);
      }
     })
     .then(function action (done) {
      console.log(changeAction)
        if (changeAction) {
          actionCall(done);
        }else{
          done();
        }
     })
     .or(function (err) {
       console.warn('PageHappy: Change Setting Error!');
       console.warn(err);
     });
  },
  /*--------------------------------------------------------------------------*/
  /*--------------------------------------------------------------------------*/
  // Sets the pages
  // @return - new page cals, called on both soft and hard update
  /*--------------------------------------------------------------------------*/
  setPages: function () {
    // console.log('SET PAGES')
    var _Ph = this,
        currentPage = _Ph.currentPage.get();
    _Ph.maxPageNumber = Math.ceil(_Ph.totalRecords.get() / _Ph.pageSize.get()) - 1;
    _Ph.multiplePages = _Ph.maxPageNumber > 0 ? true : false;
    _Ph.nextPage = currentPage + 1;
    _Ph.prevPage = currentPage - 1;
    _Ph.firstPage = currentPage === 0;
    _Ph.lastPage = currentPage === _Ph.maxPageNumber;
    
    //Reconfigures the last page to be catched
    if (_.last(_Ph.ensureCatch) === true) {
      var initEnsure = _Ph.initEnsureCatch;
      //Set back to defualt
      _Ph.ensureCatch.length = 0;
      _Ph.ensureCatch.push(initEnsure);
      _Ph.ensureCatch = _.flatten(_Ph.ensureCatch);
      //Add last page to ensureCatch
      _Ph.ensureCatch.push(_Ph.maxPageNumber);
      //Re-add boolean back on
      _Ph.ensureCatch.push(true);
    }


    //First time through set pager to ready
    if (_Ph.pass === 0) {
      _Ph.pagerReady.set(true);
    }
    //Increase Pass
    _Ph.pass++;
    if (_Ph.autoUpdate) {
      Session.set('pagerAutoUpdate', _Ph.pass);
    }

    //@-> So other funks can move along with their lifes
    return true;
  },

  //Called with true saves data
  destroy: function (deleteData) {
    var _Ph = this,
        trackers = _Ph.trackers,
        options = {};
    options.masterId = _Ph.masterId;
    options.cleanAll = true;

    //Stops all trackers
    var stopLocalTrackers = function (done) {
      _.each(trackers, function (val) {
        // debugger
        if (_Ph.retainLocalCopy) {
          var retainRef = _.map(_Ph.retainRef, function (ref) {
            return ref._id;
          });
          console.log(retainRef)
          if (_.contains(retainRef, val._id)) {
            return;
          }else{
            val.stop();
          }
        }else{
          val.stop();
        }
      });
      done();
    };


    //Removes ids from shared pager
    var dPager = function (done) {
      if (deleteData) {
        return Meteor.call('pagerDestroy', options, function (err, res) {
          if (err) {
            console.warn(err);
          }else{
            console.log(res);
            done();
          }
        });
      }
      done();
    };

    //Stops observer + del
    var dObserver = function (done) {
      // pagerClientObserver[referenceId].stop();
      // delete pagerClientObserver[referenceId];
      done();
    };

    ASQ()
      .then(function (done) {
        if (_Ph.retainLocalCopy) {
          _Ph.clientObserver.collection.pauseObservers();
        }
        done();
      })
      .gate(
        stopLocalTrackers,
        // dLocal,
        // dPager,
        dObserver
      )
      .or(function (err) {
          console.warn('Page Happy: Destroy Happy Pager Error!');
          console.warn(err);
        });
  },
  /******************************************************************************/
  /******************************************************************************/
  // Events
  /******************************************************************************/
  /******************************************************************************/
  
  pageChange: function (number) {
    console.log('PAGE CHANGe')
    var _Ph = this;
    _Ph.pagerReady.set(false);
    _Ph.currentPage.set(number);
    var setPages = _Ph.setPages();
    if (setPages) {
      _Ph.update();
    }
    //Waits for update to complete before setting pages
    //which in turn sets pagerReady
    Tracker.autorun(function () {
      if (_Ph.complete.equals('update', true)) {
        _Ph.pagerReady.set(true);
      }
    });
  },

  renderDataGenerator: function (referenceIds) {
    // console.log('RENDER DATA GENERATOR')
    var _Ph = this;    


    var formateData = function () {
      var getDoc = function (id, done) {
        var data =  _Ph.redis.get(id);
        //Edgecase Error protection
        if (data) {
          var formateData = JSON.parse(data);
          // console.log(spotlight)
          // if (_PC.spotlightActive) {
          //   return ASQ()
          //     .seq(_PC.spotlighter(formateData))
          //     .val(function (newFields) {
          //       return done(newFields);
          //     })
          //     .or(function (err) {
          //       console.warn('Current Spotlight Error!');
          //       console.warn(err);
          //     });
          // }else{
          //   return done(formateData);
          // }
          // console.log(formateData)
          return done(formateData);
        }
      };
      return ASQ()
        .map(referenceIds, getDoc)
        .val(function formateDataComplete (dataArray) {
          return dataArray;
        })
        .or(function (err) {
          console.warn('Formate Data Error!');
          console.warn(err);
        });
    };

    // var cleanOrigSpot = function (done) {
    //   //If there is a temp spotlight we need to set it to the spotlight
    //   //since the user created a `singlePage` spotlight
    //   _PC.spotlight.set(_PC.originalSpotlight.get());
    //   //Delete its after it is set since we no longer need it.
    //   delete _PC.originalSpotlight;
    //   done();
    // };

    var MGR = ASQ();
    return MGR
      // .then(function origCheck (done) {
      //   if (_PC.originalSpotlight) {
      //     var currentPage = _PC.currentPage.get(),
      //         spot = _PC.originalSpotlight.get();
      //     //If the page changes, we need to reset spotlight to the original
      //     //and only carry over those who are notr singlePaged.
      //     if (currentPage !== spot.page) {
      //       cleanOrigSpot(done);
      //     }else{
      //       done();
      //     }
      //   }else{
      //     done();
      //   }
      // })
      .seq(formateData)
      .val(function (dataArray) {
        return _Ph.renderData.set(dataArray);
      })
      .or(function (err) {
        console.warn('Render Data Generator Error!');
        console.warn(err);
      });
    
  },

  render: function () {
    var _Ph = this;
    return _Ph.renderData.get();
  },

  subReady: function () {
    var _Ph = this;
    if (_Ph.initSetUpComplete.get()) {
      var pagerReady = _Ph.pagerReady.get(),
          usingCatch = _Ph.useCatch.get(),
          subReady = _Ph.collectionHandle.ready(),
          countReady = _Ph.countHanndle.ready(),
          initLoaded = _Ph.complete.get('init');

      if (_Ph.preFetch) {
        if (pagerReady && countReady && (usingCatch || initLoaded || subReady)) {
          return true;
        }else{
          return false;
        }
      }

      if (!_Ph.preFetch) {
        if (pagerReady && subReady) {
          return true;
        }else{
          return false;
        }
      }

    }
    return false;
  },
  get: function (value) {
    var _Ph = this;
    if (_Ph.initSetUpComplete.get()) {
        var subReady = _Ph.collectionHandle.ready(),
            pagerReady = _Ph.pagerReady.get(),
            usingCatch = _Ph.useCatch.get(),
            initLoaded = _Ph.complete.get('init');

      if (pagerReady && (subReady || usingCatch || initLoaded)) {
        value = _Ph[value];
        if (value instanceof ReactiveVar) {
          return value.get();
        }else{
          return value;
        }
      }
    }
    return false;
  },

  /*--------------------------------------------------------------------------*/
  /*--------------------------------------------------------------------------*/
  // Init Setup
  // @return ->  'green light'
  /*--------------------------------------------------------------------------*/
  initSetup: function () {
    var _Ph = this;
    /**
     * Page Happy will not run unless it meets the min set of defualt data
     * @return -> 'the green light'
     */
    function checkDefaults (done) {
      var initDefaults = function (done) {
        //Collection check
        if (!(self[_Ph.collection] instanceof Mongo.Collection)) {
          //If user is using actuall  mongo ref
          if (!_.isString(_Ph.collection)) {
            throw 'Please speficy your Mongo Collection via a string rather then the refferance.';
          }
          console.warn('You must speficy what Mongo collection you would like to use.');
          //No Collection sepecified
          if (_Ph.collection === undefined) {
            throw '....And currently, you have no Mongo Collection sepecified';
          }else{
            throw _Ph.collection+' is not a Mongo Collection!';
          }
        }

        //Pager ID
        if (_Ph.masterId === undefined){
          throw 'You must speficy a masterId that is unique for this Page Happy instance';
        }

        return done();
      };
      
      return ASQ()
        .then(initDefaults)
        .val(function complete () {
          //check defaults done
          return done();
        })
        .or(function (err) {
          if (err) {
            console.warn('PageHappy: Init Setup Error! You must fix these errors to procceed!');
            console.error(err);
          }
        });
    }

    function staticDefualts (done) {
      //Assigns a Unique Id, if no accounts are present
      if (!_Ph.userId) {
        if (!_PH._GLOBAL._id) {
          _PH._GLOBAL._id = Random.id(4);
          _Ph.userId = _PH._GLOBAL._id;
        }else{
          _Ph.userId = _PH._GLOBAL._id;
        }
      }


      //Reffernace for what the initial ensure Catch was, used when 
      //the last page num is changed
      _Ph.initEnsureCatch = _.clone(_.without(_Ph.ensureCatch, true));
      
      //Ture off batch is preFetch false
      if (!_Ph.preFetch) {
        _Ph.preFetchBatch = false;
      }

      //Spotlight
      if (_.isEmpty(_Ph.spotlight)) {
        _Ph.spotlightActive = false;
      }else{
        _Ph.spotlightActive = true;
      }

      done();
    }


    function reactiveDefualts (done) {
      /*-----------------------------*/
      /// Selectors
      /*-----------------------------*/
      _Ph.query = new ReactiveDict;
      
      if (_.isEmpty(_Ph.sort)) {
        _Ph.query.set('sort', {_id: 1}); 
        _Ph.helpers().warning('It looks like you have not specified a `sort` paramater yourself. By defualt if will use {_id: 1} which will work but I do not think this is what you want.');
      }else{
        _Ph.query.set('sort', _Ph.sort);
      }
      //Set Selector
      _Ph.query.set('selector', _Ph.selector);
      //Set fields
      _Ph.query.set('fields', _Ph.fields);


      /*-----------------------------*/
      /// Page Data
      /*-----------------------------*/
      //Collection Count
      _Ph.totalRecords = new ReactiveVar(0);

      //The current page that the user is on
      _Ph.currentPage = new ReactiveVar(_Ph.currentPage);

      //Set page size / convt to recVar
      _Ph.pageSize = new ReactiveVar(_Ph.pageSize);

      // debugger
      _Ph.obvSubLimit = new ReactiveVar(_Ph.baseLimit);

      /*-----------------------------*/
      /// Query
      /*-----------------------------*/
      // if (spotlight.singlePage) {
      //   //Need to ensue settings stick
      //   var settings = _.pick(_Ph.spotlight, 'all', 'current');
      //   _Ph.originalSpotlight = new ReactiveVar(_.extend(settings, {init:true}));
      // }
      _Ph.spotlight = new ReactiveVar(_Ph.spotlight);


      /*-----------------------------*/
      /// Status - vars
      /*-----------------------------*/
      _Ph.complete = new ReactiveDict;

      //Ready Helper for template
      _Ph.pagerReady = new ReactiveVar(false);
      //Inidicated initial load data is ready to be sent
      //to the client, which is the ensureIndex data
      _Ph.complete.set('init', false);
      //If update is complete
      _Ph.complete.set('update', false);
      //Prefetch Status - true = complete
      _Ph.complete.set('preFetch', false);
      //Only true is preFetchCheck is completed
       _Ph.complete.set('preFetchCheck', false);

      //Temp block on check when ids in flux
      // _Ph.tempPreFetchCheckBlock = new ReactiveVar(false);

      /*-----------------------------*/
      /// Catch / PreFetch Vars 
      /*-----------------------------*/
      _Ph.useCatch = new ReactiveVar(false);
      //referenceIds - hold the current pages id ref
      _Ph.referenceIds = new ReactiveVar([]);

      //Pager Catched ID List 
      // _Ph.catchedDataList = new ReactiveVar([]);
      //List the pages that are currently catched
      _Ph.pagesCatched = new ReactiveVar([]);
      //Hold id ref to catched pages
      _Ph.catchedIds = new Miniredis.RedisStore(_Ph.masterId+'-catchedIds');



      //Will be the array of objs ids to check
      _Ph.preFetchCheckList = new ReactiveVar([]);


      /*-----------------------------*/
      /// Render
      /*-----------------------------*/
      _Ph.renderData = new ReactiveVar([]);

      done();
    }

    //*---------------------------------------------*/
    // Init - check/set up defualts
    /*---------------------------------------------*/
    function setDefaults () {
      return ASQ()
        .gate(
          staticDefualts,
          reactiveDefualts
        );
    }
    //Origin Control ->
    return ASQ()
      .then(checkDefaults)
      .seq(setDefaults)
      .or(function error(err) {
        console.warn('PageHappy: Setup Error!');
        console.error(err);
      });
  },
  /*--------------------------------------------------------------------------*/
  /*--------------------------------------------------------------------------*/
  // Helpers
  /*--------------------------------------------------------------------------*/
  helpers: function () {
    var _Ph = this;
    return {
      warning: function (warning) {
        if (!_Ph.override) {
          console.warn(warning);
          return console.warn('PageHappy: If you would like to remove all warnings you can add "override: true" to this specific PageHappy instance.');
        }
      },
      /**
       * Cleans redis keys
       * @param  {['p-1', 'p-2', ect.]} list
       * @return {[1, 2, 3]}
       */
      cleanRedisKeys: function (list) {
        return _.map(list, function (key) {
          return Number(key.replace( /^\D+/g, ''));
        });
      },
      /**
       * Creates a Mongo slector
       * It will either be a range or skip query
       * @param  {[array or num]} list (pages, to query)
       * @param  {[boolean]} serverCall
       * @return mongo query, or false if it is a serverCall and a
       * range selector cannot be made
       */
      mongoSelector: function (list) {
        // console.log('Range Selector')
        // console.log(arguments)
        if (!_.isArray(list) && !_.isNumber(list)) {
          console.warn('Mongo Selector Helper only works with an array');
          return false;
        }else if(_.isNumber(list)){
          //If list is just a number meaning it is only one page
          //we need to convert it into a array to advoid creating extra logic
          list = [list];
        }

        var configList = function (list) {
          // debugger
          if (list.length === 1) {
            return list;
          }

          var currentList = _.sortBy(_Ph.pagesCatched.get(), function (num) {
            return num;
          }),
          firstDiff = Math.abs((_.first(list) - _.first(currentList))),
          lastDiff = Math.abs(_.last(list) - _.last(currentList));

          //No diff
          if (firstDiff > 2 && lastDiff > 2) {
            return list;
          };

          if (firstDiff < lastDiff) {
            return list;
          }else{
            return list.reverse();
          }
        };
        /**
         * This funk is a helper to determin if the method can rangeQuery
         * rather than skip. Basically this funk figures out if the quenue list
         * is increasing or decreasing since the in order to determin if we 
         * can use rangeQuery we first have to figure out if we have data on its
         * neighboor in the correct direction.
         * @return {number} neg or positve
         */
        var findTypeOfList = function (list) {
          var listLen = list.length;
          if (listLen > 1) {
            var diffList = [];
            for (var i = 0; i < list.length; i++) {
              if (list[i+1] !== undefined) {
                diffList.push(list[i] - list[i+1]);
              }
              if (list[i-1] !== undefined) {
                diffList.push(list[i-1] - list[i]);
              }
            }
            // console.log(diffList)
            var decList = _.contains(diffList, 1),
                incList = _.contains(diffList, -1);
            //Edge case catch
            if (decList && incList) {
              return undefined;
            }
            //Grater then query
            if (decList) {
              return -1;
            }
            //Less then query
            if (incList) {
              return 1;
            }
          }else{
            //Single page fetch
            var num = (_Ph.currentPage.get() - list[0]);
            if (num > 0) {
              //LT query
              return -1;
            }else{
              //GT query
              return 1;
            }
          }
        };

        //Where the action starts
        if (_.isArray(list)) {
          // debugger
          list = configList(list);
          console.log(list)
          //set list vars
          var listType = findTypeOfList(list),
              firstPage = _.first(list),
              listCount = list.length,
              pageSize = _Ph.pageSize.get(),
              prePage = firstPage - listType,
              key = "p-" + (prePage),
              exists = _Ph.catchedIds.exists(key),
              lastRangeId,
              rangeValue;

          //GT query
          if (exists && listType === 1) {
            lastRangeId = _.first(_Ph.catchedIds.lrange(key, (pageSize - 1), pageSize));
            return {
              firstPage: firstPage,
              lastRangeId: lastRangeId,
              rangeKey: '$gt',
              rangeLimit: pageSize * listCount,
              batchList: list
            };
          }
          
          //LT query
          if (exists && listType === -1) {
            lastRangeId = _.first(_Ph.catchedIds.lrange(key, 0, 1));
            return {
              firstPage: firstPage,
              lastRangeId: lastRangeId,
              rangeKey: '$lt',
              rangeLimit: pageSize * listCount,
              batchList: list
            };
          }

          if (_.contains(_Ph.pagesCatched.get(), firstPage - 1)) {};

          // //Method will config skip
          // if(serverCall){
          //   return {
          //     firstPage: firstPage,

          //   }
          // }

          //Skip query for client
          return {
            firstPage: firstPage,
            skip: firstPage * pageSize,
            limit: listCount * pageSize,
            batchList: list
          }; 
        }else{
          console.warn('Mongo Selector Helper only works with an array');
          return false;
        }
      },


      /*-----------------------------*/
      /// Create Batch Queue
      /*-----------------------------*/
      createBatchQueue: function (list) {
          var batchQueue = [],
              batchCount = 0;

          var generateOrderedList = function (list) {
            return list = _.sortBy(list, function (num) {
              return num;
            });
          };
        
          /**
           * Creates batches
           * @param  {[#]}
           * @return {
           *   batchList: sequance of numbers (cannot be missing a num)
           *   throwList: all the numbs of the list that did not make it in the
           *   batchList
           * }
           */
          var createBatch = function (list) {
            var batchList = [],
                throwList = [];
            if (list.length === 1) {
              return {
                batchList: list,
                throwList: []
              };
            }

            //Iterates through list, if next number is missing it is thrown
            //otherwise it will be pushed to batchList
            for (var i = _.first(list), j = i, q = 0; i < (j+list.length); i++, q++) {
               if (list[q] === i && q < _Ph.preFetchCheckBatchSize) {
                  batchList.push(list[q]);
               }else{
                throwList.push(list[q]);
               }
            }
            return{
              batchList: batchList,
              throwList: throwList
            };
          };

        /**
         * Inserst batches into batchQuene
         * If it is complete is false its will loop this whole process
         * again until there all nums are put into batches
         */
        var generateBatchQueue = function (loopList) {
          var batch = createBatch(loopList);
          if (!_.isEmpty(batch.batchList)) {
            batchQueue.push(batch.batchList);
            batchCount++;
          }

          if (!_.isEmpty(batch.throwList)) {
            //Remove pages that have a batch
            var modifiedList = _.map(loopList, function (val) {
              if(!_.contains(batch.batchList, val)){
                return val;
              }
            });
            return {
              complete: false,
              modifiedList: _.without(modifiedList, undefined)
            };
          }else{
            return {
              complete: true,
            };
          }
        };

        return ASQ()
          .val(generateOrderedList(list))
          .then(function (done, list) {
            //Loops funk depending on val
            var nextBatch = function (loopList) {
              //Set looplist
              loopList = _.isArray(loopList) ? loopList : list;
              //Loop
              var MGR = ASQ();
              return MGR
                .val(generateBatchQueue(loopList))
                .then(function loop (done, list) {
                  if (!list.complete) {
                    nextBatch(list.modifiedList);
                  }else{
                    done();
                  }
                })
                .val(function complete () {
                  //Parent -> Done
                  done();
                });
            };

            ASQ()
              .seq(nextBatch(done, list));

          })
          .val(function complete () {
            return batchQueue;
          })
          .or(function (err) {
            console.warn('Page-Happy: PreFetchCheck --> createBatchQueue Erorr!');
            console.warn(err);
          });
      },




      /*-----------------------------*/
      /// Get Id List
      /*-----------------------------*/
      getIdList: function (params) {
        var collection = LocalPH,
            selector = params.selector,
            // userId = Meteor.userId() || 'user',
            options = {
              sort: params.sort,
              fields: {_id: 1},
              limit: params.pageSize
          },
          reverseList = false;
    
        (function config () {
          // debugger
          
          //List starts from init index
          if (params.initList) {
            options.limit = (params.list.count * params.pageSize);
          }

          if (params.lastRangeId) {
            console.log('RAAAANNNNNNNNNNNNNNNGGGG')
            var range = {}, key, value,
                rangeSelector = {},
                fields = {},
                sortKey = _.first(_.keys(options.sort)),
                sortVal = _.first(_.values(options.sort));
            fields[sortKey] = 1;

            //Determins the key, for the range
            if (params.rangeKey) {
              key = params.rangeKey;
            }else{
              sortVal === 1 ? key = '$gt' : key = '$lt';
            }

            //If this is the case we have to alter the sort
            //since it will be doing the reverse
            if (sortVal === 1 && key === '$lt') {
              options.sort[sortKey] = -1;
              reverseList = true;
            }
            if(sortVal === -1 && key === '$gt'){
              options.sort[sortKey] = 1;
              reverseList = true;
            }

            if (_.isNumber(params.rangeLimit)) {
              options.limit = params.rangeLimit;
            }

            value = collection.findOne({_id: params.lastRangeId}, {fields: fields});

            range[key] = value[sortKey];
            rangeSelector[sortKey] = range;
            selector = _.extend(selector, rangeSelector);
          }

          //Last Default
          if (!params.initList && !params.lastRangeId) {
            console.log('SKKKKKKKKKKKKKKKKKKKKKKKKIP')
            options.skip = params.currentPage * params.pageSize;
            if (params.batchMode) {
              options.skip = _.first(params.currentPage) * params.pageSize;
              options.limit = (params.currentPage.length * params.pageSize);
            }
          };

        })();

        //Get cursor && complie list
        var cursor = collection.find(selector, options),
            idList = cursor.map(function (doc) {
              return doc._id;
            });

        //Reverse List before pushing
        if (reverseList) {
          idList.reverse();
        }


        return {
          list: params.list,
          referenceIds: idList
        };

      }
    };
  }
};


/******************************************************************************/
/******************************************************************************/
// Method Calls
/******************************************************************************/
/******************************************************************************/
Meteor.methods({
  //Removes ids from pager collection
  pagerCleaner: function (ids, options) {
    console.log('PAGE CLEANER METHOD')
    var cleanCursor;
    
      //removes old data
      if (options.cleanOld) {
        cleanCursor = Pager.find({referenceId: {$nin: ids}}, options);
        cleanCursor.forEach(function (doc) {
          Pager.remove(doc._id);
        });
        return true;
      }


      //For cleanup when catch limit is hit
      cleanCursor = Pager.find({referenceId: {$in: ids}}, options);
      cleanCursor.forEach(function (doc) {
        Pager.remove(doc._id);
      });
      return true;
  },
    pagerPreFetchCheck: function (params) {
      //Extract varibles from optiosn
      var collection = self[params.collection],
          selector = params.selector,
          options = {
            sort: params.sort || {},
            fields: {_id: 1},
            skip: params.currentPage * params.pageSize,
            limit: params.pageSize
        };

        var currentIdSet = collection.find(selector, options),
            idsToCheck = params.idsToCheck,
            newIdSet = currentIdSet.map(function (doc) {
              return doc._id;
            });

        var compareIdList = function (arr1, arr2) {
          if (arr1.length === arr2.length) {
            var matchPairs = [];
            for (var i = 0; i < arr1.length; i++) {
              matchPairs.push(arr1[i] === arr2[i]);
            }
            //Will only return true if all pairs match
            return _.every(matchPairs, _.identity);
          }else{
            return false;
          }
        };

        if (compareIdList(idsToCheck, newIdSet)) {
          return {
            setNewPrefetch: false
          };
        }else{
          return {
            setNewPrefetch: true,
            newIdSet: newIdSet,
            currentPage: params.currentPage
          };
        }
    },
});














/*--------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------*/
// Old Trackers
/*--------------------------------------------------------------------------*/
      /*-----------------------------*/
      /// Pre Fetch Catch Check
      /*-----------------------------*/
      // if (_PC.autoUpdatePreFetch) {


      //   //Watches list which is created at _PC.autoUpdateCheck
      //   var callPreFetchCheck = function (checkList) {
      //     return _PC.preFetchCheck(checkList);
      //   };
      //   var preFetchCheck = _.debounce(callPreFetchCheck, _PC.autoUpdatePreFetchDebounce);
      //   var t1 = Tracker.autorun(function () {
      //     var checkList = _PC.preFetchCheckList.get(),
      //         fetchCheckComplete = _PC.preFetchCheckComplete.get();
      //     //Don't call if in progress or with an empty list
      //     if (fetchCheckComplete && !_.isEmpty(checkList) && !_PC.tempPreFetchCheckBlock.get()) {
      //         preFetchCheck(checkList);
      //     }
      //   });
      //   _PC.trackers.push(t1);



      //   //Resets Block (refer to autorun bellow)
      //   var resetBlock = function () {
      //     return Meteor.setTimeout(function () {
      //       _PC.tempPreFetchCheckBlock.set(false);
      //     },  _PC.autoUpdatePreFetchDebounce*2);
      //   };
      //   //Watches preFetchBlock, this is set to true if there is a change
      //   //in pager settings since we do not want to check ids that are in flux
      //   var t2 = Tracker.autorun(function () {
      //     var tempBlock = _PC.tempPreFetchCheckBlock.get();
      //     if (tempBlock) {
      //       resetBlock();
      //     }
      //   });
      //   _PC.trackers.push(t2);

      // }


      /*-----------------------------*/
      /// Observer Selector
      /*-----------------------------*/
      // var t3 = Tracker.autorun(function () {
      //   // var catchedDataList = _PC.catchedDataList.get();
      //   if (_PC.user) {
      //     var userId = Meteor.userId();
      //     _PC.obvSelector.set({$and: [
      //       {masterId: _PC.masterId},
      //       {userId: userId}
      //     ]});
      //   }else{
      //     //No users
      //     // _PC.obvSelector.set({referenceId: {$in: catchedDataList}});
      //     _PC.obvSelector.set({masterId: _PC.masterId});
      //   }
      // });
      // _PC.trackers.push(t3);
      



      /*-----------------------------*/
      /// Cleans collection
      /*-----------------------------*/
      // if (_PC.cleanCollectionCatch) {
      //   var cleanCatch = function () {
      //       _PC.cleanComplete.set(false);
      //       _PC.cleanCatch();
      //   };
      //   /**
      //    * When the catch limit has been hit this funk controlls the catch
      //    * cleaner process
      //    */
      //   var t6 = Tracker.autorun(function () {
      //     var currentCatchSize = LocalCol[_PC.ID].find(_PC.obvSelector.get()).count(),
      //         pageSize = _PC.pageSize.get(),
      //         catchLimit = _PC.obvSubLimit.get(),
      //         cleanTrigger = _PC.cleanTrigger || Math.ceil(catchLimit*0.75),
      //         cleanComplete = _PC.cleanComplete.get();

      //     // console.log(_PC.pass)
      //     //To clean or not to clean. That is the question.
      //     //depends on cleanComplete so shit don't get fucked and people wait their turn.
      //     if (currentCatchSize >= cleanTrigger && cleanComplete && _PC.preFetch) {
      //       cleanCatch();
      //     }

      //   });
      //   _PC.trackers.push(t6);

      //     /**
      //      * **************FIGURE OUT A BETTER WAY ***********************
      //      * Inital Clean of collection
      //      * This checks to see if the localCollection does not have any data
      //      * it should not have. After the check is complete the tracker and 
      //      * interval will be cleaned.
      //      * The only real reason this is here for when users refresh their page
      //      * and by doing so the old data is retained in the local collection
      //      * with incorrect reffrences. It does not cause any harm per-se
      //      * but by cleaning it will increase latter speeds, or atleast I think.
      //      */
      //     if (!_PC.cleanCollectionOnLoad && _PC.preFetch) {
      //       var subsLoaded = new ReactiveVar(false),
      //           initPreFetchComplete = function () {
      //             var loaded = _PC.catchedData.get().length === _PC.ensureCatch.length;
      //             if (_PC.ensureCatch >= _PC.initSubLimit) {
      //               subsLoaded.set(loaded);
      //             }
      //             subsLoaded.set(loaded);
      //           },
      //           resetCatch = function () {
      //             _PC.cleanComplete.set(false);
      //             _PC.cleanCatch(true);
      //           },
      //           //?? do we need???
      //           genInterval = function () {
      //             var inter = _PC.autoUpdatePollPeriod - 2500;
      //             if (0 > inter) {
      //               inter = 10;
      //             }
      //             return 1000;
      //           },
      //           subLoadedCheck = Meteor.setInterval(function () {
      //             initPreFetchComplete();
      //           }, genInterval());

      //         var t7 = Tracker.autorun(function (initClean) {
      //           if (subsLoaded.get()) {
      //             var corretCollectionSize = _PC.catchedData.get().length * (_PC.pageSize.get()),
      //                 currentCollectionSize = LocalCol[_PC.ID].find().count(),
      //                 //Cleans up tracker and such
      //                 cleanTracker = function () {
      //                   Meteor.clearInterval(subLoadedCheck);
      //                   initClean.stop();
      //                 };
      //             //If the calc is not right clean the collection
      //             if (corretCollectionSize !== currentCollectionSize) {
      //               resetCatch();
      //               cleanTracker();
      //             }
      //             cleanTracker();
      //           }
      //         });
      //       _PC.trackers.push(t7);
      //     }


      // }

      /*-----------------------------*/
      /// Render Data Generator
      /*-----------------------------*/
      // var pageTracker;
      // Tracker.autorun(function () {
      //   console.log('RENDER Tracker')
      //   var currentPage = _PC.currentPage.get();
      //   console.log(currentPage);
      //   console.log(pageTracker)
      //   if (currentPage !== pageTracker) {
      //     _PC.renderDataGenerator();
      //     pageTracker = currentPage;
      //   }
      // });