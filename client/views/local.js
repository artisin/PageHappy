/******************************************************************************/
/******************************************************************************/
/*  Controller:  
/*  Template: /client/views/templateLocation
/******************************************************************************/
/******************************************************************************/

/***************************************************************/
/* Template States */
/***************************************************************/
/*******************************/
// Created
/*******************************/
Template.local.created = function () {
this.pager = PagerCreator({
    pagerMasterId: 'heavan1',
    collection: 'Heavan',
    pageSize: 10,
    catchLimit: 500,
    cleanTrigger: 400,
    querySpotlight: {

    },
    querySort: {
      number: -1
    },
  });
};

/*******************************/
// Rendered
/*******************************/
Template.local.rendered = function () {
};

/*******************************/
// Destroyed
/*******************************/
Template.local.destroyed = function () {
  //If true will not remove data from collection if you want
  //to use the data later lets and not reload it
  this.pager.destroy(true);
};


/***************************************************************/
/* Template Helpers */
/***************************************************************/
Template.local.helpers({
  local: function () {
    // return heavan.find().count();
  },
  pager: function () {
    return Pager.find().count();
  },
  people: function () {
    // return People.find().count();
  },
  pages: function () {
    var pagesLoaded = Session.get('pagesLoaded'),
        current = Session.get('currentPage');
    // console.log(pagesLoaded)
    // console.log(current)
    var pageInfo = _.map(pagesLoaded, function (num) {
      if (num === current) {
        return {
          num: num,
          current: 'red'
        };
      }else{
        return {
          num: num,
          current: 'teal'
        };
      }
    });
    // console.log(pageInfo)
    return _.sortBy(pageInfo, function (val) {
      return val.num;
    });
  },
  localData:function () {
    var cursor = LocalPager.find();
    return cursor.map(function (doc) {
      return {
        _id: doc._id,
        number: doc.number,
        pagerId: doc.pagerId,
        name: doc.firstName,
        page: Math.floor(doc.number * 0.10)+1
      };
    });
    
  },
  context: function () {
    var pager = Template.instance().pager;
    return pager;
  },
  subReady: function () {
    var pager = Template.instance().pager;
    return pager.subReady();
  },
  page: function () {
    var pager = Template.instance().pager;
    // console.log('RENDER+++++++++++++++++++++++++++++++++++++++++')
    // pager.render();
    return pager.render();

    // return collection.map(function (doc) {
    //  return {
    //     firstName: doc.firstName,
    //     number: doc.number,
    //     highlight: doc.highlight === true ? 'highlight' : '',
    //     odd: doc.odd
    //   };
    // });
  },
});


/***************************************************************/
/* Template Events */
/***************************************************************/
Template.local.events({
  
});