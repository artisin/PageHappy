var Future = Npm.require('fibers/future'),
    self = this;


/*---------------------------------------------*/
// Helper
/*---------------------------------------------*/
function pagerAsync(cb, params){
  return cb(null, getPageHappy(params));
}


/*---------------------------------------------*/
// Methods
/*---------------------------------------------*/
Meteor.methods({
  getPagerAsync: function (params) {
    this.unblock();
    var fut = new Future();
    var bound_callback = Meteor.bindEnvironment(function (err, res) {
      if(err) {
        fut.throw(err);
      }  else {
        fut.return(res);
      }
    });
    pagerAsync(bound_callback, params);
    return fut.wait();
  },
  // pagerDestroy: function (options) {
  //   this.unblock();
  //   //Cleans all with 'x' masterId - used in destroy call
  //   var cleanCursor = Pager.find({pagerMasterId: options.masterId});
  //   var cleaned = [];
  //   cleanCursor.forEach(function (doc) {
  //     cleaned.push(doc._id);
  //     Pager.remove(doc._id);
  //   });
  //   return cleaned;
  // },
  //Removes ids from pager collection
  pageHappyCleaner: function (observerId) {
    console.log('PAGE CLEANER METHOD')
    // this.defer();
    SharedPH.remove({observerId: observerId})
    },
    pagerPreFetchCheck: function (params) {
      this.unblock();
      //Extract varibles from optiosn
      var collection = self[params.collection],
          selector = params.selector,
          options = {
            sort: params.sort || {},
            fields: {_id: 1},
            skip: params.currentPage * params.pageSize,
            limit: params.pageSize
        };

        //BatchMode set
        if (params.batchMode) {
          options.skip = _.first(params.currentPage) * params.pageSize;
          options.limit = (params.currentPage.length * params.pageSize);
          console.log(options);
        }

        var currentIdSet = collection.find(selector, options),
            idsToCheck = params.idsToCheck,
            newIdSet = currentIdSet.map(function (doc) {
              return doc._id;
            });

        var compareIdList = function (arr1, arr2) {
          if (arr1.length === arr2.length) {
            var matchPairs = [];
            for (var i = 0; i < arr1.length; i++) {
              matchPairs.push(arr1[i] === arr2[i]);
            }
            //Will only return true if all pairs match
            return _.every(matchPairs, _.identity);
          }else{
            return false;
          }
        };

        if (compareIdList(idsToCheck, newIdSet)) {
          return {
            setNewPrefetch: false
          };
        }else{
          return {
            setNewPrefetch: true,
            newIdSet: newIdSet,
            currentPage: params.currentPage,
            batchMode: params.batchMode
          };
        }

    },
    // getCurrentPhSet: function (params) {
    //   //Extract varibles from optiosn
    //   var collection = self[params.collection],
    //       selector = params.selector,
    //       options = {
    //         sort: params.sort || {},
    //         fields: {_id: 1},
    //         skip: params.currentPage * params.pageSize,
    //         limit: params.pageSize
    //     };
    //     var idSet = collection.find(selector, options),
    //         count = idSet.count();
    //     idSet = idSet.map(function (doc) {
    //       return doc._id;
    //     });
    //   return {
    //     totalRecords: count,
    //     pagerId: idSet
    //   };
    // }
});



observers = {

};

SharedPH = new Mongo.Collection('sharedPh');
SharedPH._ensureIndex({referenceId: 1});

this.getPageHappy = function (params) {
  //Extract varibles from optiosn
  var collection = this[params.collection],
      selector = params.selector,
      masterId = params.masterId,
      userId = params.userId,
      // sub = observers.userId.masterId,
      options = {
        sort: params.sort,
        fields: params.fields,
        limit: params.pageSize,
    },
    reverseList = false;


    (function config () {
      //Query Type
      if (params.lastRangeId) {
        // console.log('QUUERYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY')
        //First pass randeId will be undefined, since there is
        //no need to range on first due to inharent limit
        if (params.lastRangeId !== undefined) {
          var range = {}, key, value,
              rangeSelector = {},
              fields = {},
              sortKey = _.first(_.keys(options.sort)),
              sortVal = _.first(_.values(options.sort));
          fields[sortKey] = 1;

          //Determins the key, for the range
          if (params.rangeKey) {
            key = params.rangeKey;
          }else{
            sortVal === 1 ? key = '$gt' : key = '$lt';
          }

          //If this is the case we have to alter the sort
          //since it will be doing the reverse
          if (sortVal === 1 && key === '$lt') {
            options.sort[sortKey] = -1;
            reverseList = true;
          }
          if(sortVal === -1 && key === '$gt'){
            options.sort[sortKey] = 1;
            reverseList = true;
          }

          //This value is for the range.
          //ex. if the last range i
          value = collection.findOne({_id: params.lastRangeId}, {fields: fields});

          range[key] = value[sortKey];
          rangeSelector[sortKey] = range;
          selector = {$and: [selector, rangeSelector]};
        }
        //Limit Config
        if (params.batchMode) {
          options.limit = (params.currentPage.length * params.pageSize);
        }
      }else{
        //Skip
        console.log('SKKKKKKKKKKKKKKKKKKKKKKKKIP')
        options.skip = params.currentPage * params.pageSize;
        if (params.batchMode) {
          options.skip = _.first(params.currentPage) * params.pageSize;
          options.limit = (params.currentPage.length * params.pageSize);
        }
      }


    })();


  var subPointer = observers[userId],
      sub = subPointer[masterId];
  //Tracker paused unitl data is sent is formated and added to miniRedis
  // PageHappyTracker.pauseObservers();
  //Count is added for miniredis to observe in order
  
  console.log(selector)
  console.log(options)
  var idList = [];
  collection.find(selector, options).forEach(function (doc) {
    var id = doc._id,
        fields = _.omit(doc, '_id');
    idList.push(id);
    var defualts = {
      referenceId: id,
      masterId: masterId
    };

    var doc = _.extend(fields, defualts);


    sub.added('localPh', id, doc);



  });
  
  // .observeChanges({
  //   added: function (id, fields) {
  //     console.log('OBV')
      // idList.push(id);
      // var defualts = {
      //   referenceId: id,
      //   masterId: masterId,
      //   observerId: observerId
      //   // userId: userId,
      // };
  //     var doc = _.deepExtend(fields, defualts);
  //     SharedPH.insert(doc);
  //   }
  // });

  //Tracker rusmed
  // PageHappyTracker.resumeObservers(); 

  //Reverse List before pushing
  if (reverseList) {
    console.log('REVERSE');
    params.reverse = true;
    // idList.reverse();
  }else{
    params.reverse = false;
  }


  return {
    pgNum: params.currentPage,
    referenceIds: idList,
    reverse: params.reverse
  };

};





      

var self = this;
Meteor.publish('localPager', function (params) {
  var sub = this,
      collection = self[params.collection],
      masterId = params.masterId,
      userId = params.userId,
      limit = params.limit,
      sort = params.sort,
      initializing = true;
      // userId = this.userId || 'user';
      // pageSize = params.pageSize,
      // currentPage = params.currentPage,
      // listIds = [];

    var k;
    //If no ref to 
    if (!observers[userId]) {
      observers[userId] = {};
      k = observers[userId];
      if (!k[masterId]) {
        k[masterId] = this;
      }
    }else{
      k = observers[userId];
      if (!k[masterId]) {
        k[masterId] = this;
      }else{
        k[masterId] = this;
      }
    }



    var observerHandle = 
    SharedPH
    .find({observerId: userId}, {sort: {number: 1}, limit: limit})
    .observe({
      added: function (doc) {
        // console.log('rAdded')
        // console.log(arguments)
        var id = doc.referenceId;
        doc = _.omit(doc, '_id');
            // console.log(newDoc.number)
        sub.added('localPh', id, doc);
      },
      changed: function () {
        console.log('rChange-----------------------------------------')
        // console.log(doc.value)
        // console.log(oldDoc.value)
        // if (doc.value !== oldDoc.value) {
        //   var newDoc = JSON.parse(doc.value),
        //       id = newDoc.referenceId;
        //   sub.changed('localPh', id, newDoc);
        // }
      },
      removed: function () {
        console.log('rRemoved-------------------------------------------')
        // console.log(arguments)
        // var oldDoc = JSON.parse(doc.value);
        // var id = oldDoc.referenceId;
        // console.log(id)
        // sub.removed('localPh', id);
      }
    });
    //Init data has been sent so indicate
    //the sub is ready
    sub.ready();



    var mongoHandle =  collection
      .find({}, {sort: sort})
      .observe({
        added: function (doc) {
          if (initializing) {
            return;
          }
          console.log('mAdded')
          console.log(doc)
          var id = doc._id;
          var defualts = {
            referenceId: id,
            masterId: masterId,
            changed: true
            // userId: userId,
          };
          doc = _.omit(doc, '_id');

          var newDoc = _.deepExtend(defualts, doc);
          sub.added('localPh', id, newDoc);
        },
        changed: function (doc, oldDoc) {
          console.log('mChanged')
          var defualts = {
            referenceId: doc._id,
            masterId: masterId,
          };
          doc = _.deepExtend(defualts, doc);
          sub.changed('localPh', oldDoc._id, doc);
        },
        removed: function (doc) {
          console.log('mRemoved')
          console.log(arguments)
          sub.removed('localPh', doc._id);
        }
      });
      initializing = false;


  sub.onStop(function () {
    console.log('STOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOP')
    mongoHandle.stop();
    observerHandle.stop();
    Meteor.call('pageHappyCleaner', userId, function (err) {
      if (err) {
        console.log(err);
      }
    });
  });

  
});


Meteor.publish('collectionCount', function(params) {
  console.log('COLLECTION COUNT SUB')
  var collection = self[params.collection], 
      sub = this,
      masterId = params.masterId,
      selector = params.selector,
      options = {
        fields: {_id: 1},
      },
      cursor = collection.find(selector, options),
      count = 0,
      initializing = true;

  var countHandle = 
    cursor
    .observeChanges({
      added: function () {
        count++;
        if (!initializing) {
          sub.changed('countPh', masterId, {count: count});
        }
      },
      removed: function () {
        count--; 
        sub.changed('countPh', masterId, {count: count});
      }
    });
  
  if (initializing) {
    sub.added('countPh', masterId, {count: cursor.count()});
  }

  initializing = false;
  sub.onStop(function () {
    console.log('Count Stop')
    countHandle.stop();
  });
  sub.ready();
});
