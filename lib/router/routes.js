/***************************************************************/
/* Config Routes */
/***************************************************************/

/*******************************/
/* Global Config */
/*******************************/

Router.configure({
  layoutTemplate: 'DefualtLayout',
  loadingTemplate: 'Loading',
});


/*******************************/
/* Index Route */
/*******************************/
Router.route('/', {
  name: 'index.link',
  template: 'index',
});

//*-----------------------------*/
/// Examples
/*-----------------------------*/
Router.route('/examples', {
  name: 'examples.link',
  template: 'examples',
});


Router.route('/examples/insight', {
  name: 'insight.link',
  template: 'insight',
});

Router.route('/examples/spotlight', {
  name: 'spotlight.link',
  template: 'spotlight',
});


Router.route('/next', {
  name: 'nextPage.link',
  template: 'nextPage',
  //controller: 'index_Controller'
});



Router.route('/redis', {
  name: 'redis.link',
  template: 'redis',
  //controller: 'index_Controller'
});

Router.route('/addUser', {
  name: 'adduser.link',
  template: 'adduser',
  //controller: 'index_Controller'
});
